package rs.hump.aboutmy;

import com.orm.Database;
import com.orm.SugarApp;

import rs.hump.aboutmy.database.EncryptedDatabase;

/**
 * Used to init Joda time library
 *
 * @author Dejan Markovic
 */
public class AboutMyApplication extends SugarApp {

    private EncryptedDatabase myEncryptedDatabase;

    @Override
    public void onTerminate() {
        if(this.myEncryptedDatabase != null) {
            this.myEncryptedDatabase.getDB().close();
        }

        super.onTerminate();
    }

    public void setEncryptionKey(String key) {
        myEncryptedDatabase = new EncryptedDatabase(getSugarContext(), key);
    }

    @Override
    protected Database getDatabase() {
        return myEncryptedDatabase;
    }

    public Database tryGetDatabase() {
        return getDatabase();
    }

    public void closeDatabaseConnection(){
        myEncryptedDatabase.getDB().close();
    }
}
