package rs.hump.aboutmy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import rs.hump.aboutmy.domain.attributes.Attribute;
import rs.hump.aboutmy.domain.attributes.Birthday;
import rs.hump.aboutmy.domain.attributes.FavouriteBook;
import rs.hump.aboutmy.domain.attributes.FavouriteColor;
import rs.hump.aboutmy.domain.attributes.FavouriteDrink;
import rs.hump.aboutmy.domain.attributes.FavouriteFlowers;
import rs.hump.aboutmy.domain.attributes.FavouriteFood;
import rs.hump.aboutmy.domain.attributes.FavouriteMusic;
import rs.hump.aboutmy.domain.attributes.Height;
import rs.hump.aboutmy.domain.attributes.MenstrualCalendar;
import rs.hump.aboutmy.domain.attributes.ShirtSize;
import rs.hump.aboutmy.domain.attributes.ShoeSize;
import rs.hump.aboutmy.domain.attributes.TShirtSize;
import rs.hump.aboutmy.domain.attributes.TrousersSize;
import rs.hump.aboutmy.domain.attributes.WeddingDay;
import rs.hump.aboutmy.domain.attributes.Weight;
import rs.hump.aboutmy.util.Config;

/**
 * All the constants in project are here.
 *
 * @author Dejan Markovic
 */
public class Constants {

    public static final long DAY = 24 * 60 * 60 * 1000;

    public static DateFormat dateFormat;

    public static Config config = new Config();

    public static final String[] relations = {"Girlfriend", "Boyfriend", "Lover", "Friend", "Spouse", "Mother", "Father",
            "Grandmother", "Grandfather", "Sister", "Brother"};

    public static ArrayList<Attribute> attributes = new ArrayList<Attribute>();

    static {
        attributes.add(new Birthday());
        attributes.add(new WeddingDay());
        attributes.add(new Height());
        attributes.add(new Weight());
        attributes.add(new FavouriteFlowers());
        attributes.add(new ShoeSize());
        attributes.add(new ShirtSize());
        attributes.add(new FavouriteColor());
        attributes.add(new MenstrualCalendar());
        attributes.add(new TrousersSize());
        attributes.add(new TShirtSize());
        attributes.add(new FavouriteBook());
        attributes.add(new FavouriteDrink());
        attributes.add(new FavouriteFood());
        attributes.add(new FavouriteMusic());
    }
}
