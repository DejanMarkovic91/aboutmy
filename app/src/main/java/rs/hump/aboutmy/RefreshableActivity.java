package rs.hump.aboutmy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.Database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rs.hump.aboutmy.domain.AbstractNotification;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.domain.attributes.TipMessage;
import rs.hump.aboutmy.notifications.NotificationTimeReceiver;
import rs.hump.aboutmy.profile.MainActivity;

/**
 * Base activity for all activities that need authentication of user.
 *
 * @author Dejan Markovic
 */
public abstract class RefreshableActivity extends RoboAppCompatActivity {

    private static MaterialDialog dialog;
    private static ProgressDialog myProgressDialog;
    protected static String PIN;
    private static ArrayList<Notification> notifications = null;

    public abstract void refresh(String PIN);

    protected static void showLoginDialog(final RefreshableActivity activity, boolean isRegistering) {
        myProgressDialog = new ProgressDialog(activity);
        myProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        myProgressDialog.setMessage(activity.getString(R.string.loading_please_wait));
        myProgressDialog.setCancelable(false);
        myProgressDialog.show();
        if (dialog == null && isRegistering) {
            dialog = new MaterialDialog.Builder(activity)
                    .content(isRegistering ? R.string.register_input_content : R.string.input_content)
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                    .input(R.string.pin, R.string.input_prefill, new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(MaterialDialog dialog, CharSequence input) {
                                    AboutMyApplication app = ((AboutMyApplication) (AboutMyApplication.getSugarContext()));
                                    app.setEncryptionKey(input.toString());
                                    if (input.toString().equals("")) dialog.dismiss();
                                    try {
                                        myProgressDialog.show();
                                        dialog.dismiss();
                                        Database database = app.tryGetDatabase();
                                        database.getDB();
                                        PIN = input.toString();
                                        activity.refresh(input.toString());
                                    } catch (Exception e) {
                                        //wrong password
                                        dialog.show();
                                        e.printStackTrace();
                                        Toast.makeText(activity, activity.getString(R.string.error_wrong_pin), Toast.LENGTH_SHORT).show();
                                    } finally {
                                        myProgressDialog.dismiss();
                                    }
                                }
                            }
                    )
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction
                                            which) {
                                        dialog.dismiss();
                                        activity.finish();
                                    }
                                }
                    )
                    .autoDismiss(false)
                    .cancelable(false)
                    .negativeText(android.R.string.cancel)
                    .build();
            dialog.show();
        }
        myProgressDialog.dismiss();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //addListeners();
        if (PIN == null) {
            //on demand authentication
            final File db = new File(getFilesDir().getParent() + "/databases/about.db");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RefreshableActivity.showLoginDialog(RefreshableActivity.this, !db.exists());
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    List<TipMessage> messages = null;
                    try {
                        messages = TipMessage.listAll(TipMessage.class);
                    } catch (Exception e) {

                    }
                    if (messages == null || messages.isEmpty()) {
                        // read from raw file
                        InputStream inputStream = RefreshableActivity.this.getResources().openRawResource(R.raw.tips);

                        InputStreamReader inputreader = new InputStreamReader(inputStream);
                        BufferedReader buffreader = new BufferedReader(inputreader);
                        String line;
                        StringBuilder text = new StringBuilder();

                        try {
                            while ((line = buffreader.readLine()) != null) {
                                String[] ln = line.split("#");
                                TipMessage tip = new TipMessage();
                                tip.setMessage(ln[0]);
                                tip.setRelation(ln[1]);
                                tip.setGender(ln[2]);
                                tip.save();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 1000);
        } else {
            //save PIN
            refresh(PIN);
            rescheduleNotifications();
        }

    }

    private void rescheduleNotifications() {
        ArrayList<Long> notifications = null;
        try {
            FileInputStream fos = openFileInput(NotificationTimeReceiver.PENDING_NOTIFICATIONS_FILE);
            ObjectInputStream in = new ObjectInputStream(fos);
            notifications = (ArrayList<Long>) in.readObject();
            in.close();
        } catch (ClassNotFoundException | IOException e) {
            Log.e(NotificationTimeReceiver.class.getName(), e.getMessage());
            notifications = new ArrayList<>();
        }
        //if there are actual notifications to be rescheduled
        if (!notifications.isEmpty()) {
            try {
                Log.i(RefreshableActivity.class.getName(), "Rescheduling notifications: " + notifications.size());
                Iterator<Long> iterator = notifications.iterator();
                while (iterator.hasNext()) {
                    //fetch old notification
                    List<Notification> n = Notification.find(Notification.class, "id = ?", String.valueOf(iterator.next()));
                    if (n.isEmpty()) {
                        Log.e(RefreshableActivity.class.getName(), "ERROR: Notification not found!");
                        continue;//notification not found
                    }
                    Notification notification = n.get(0);
                    Log.i(RefreshableActivity.class.getName(), "Rescheduling notification: " + notification);
                    if (notification.getPeriod() == 0) {
                        //single time notification do not reschedule
                        continue;
                    }

                    //else we reschedule
                    AbstractNotification notif = new AbstractNotification();
                    notif.setDue(notification.getDueTime() + notification.getPeriod());
                    notif.setPrivateProfile(notification.getProfile().getPrivateProfile());
                    //create rescheduled notification
                    Notification rescheduledNotification = new Notification();
                    rescheduledNotification.setDueTime(notif.getDue());
                    rescheduledNotification.setMessage(notification.getMessage());
                    rescheduledNotification.setLength(notification.getLength());
                    rescheduledNotification.setPeriod(notification.getPeriod());
                    rescheduledNotification.setProfile(notification.getProfile());
                    if (!notification.getProfile().containsAttribute(notification.getAttributeId())) {
                        continue;
                    }
                    rescheduledNotification.setAttributeId(notification.getAttributeId());
                    //get new random tip
                    TipMessage.findTipMessageAndPutInNotification(notification.getProfile(), rescheduledNotification);
                    //save rescheduled notification and add it to profile
                    if (rescheduledNotification.getProfile().getPrivateProfile() == 1) {
                        rescheduledNotification.save();
                    } else {
                        List<Notification> notifications1 = RefreshableActivity.readAllNotes(this);
                        notifications1.add(rescheduledNotification);
                        long newId = -1;
                        for (Notification n1 : notifications1) {
                            if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
                        }
                        newId--;
                        rescheduledNotification.setId(newId);
                        RefreshableActivity.writeAllNotes(this, notifications1);
                    }
                    notif.setNotificationId(rescheduledNotification.getId());
                    notification.getProfile().getNotifications().add(rescheduledNotification);
                    MainActivity.addNotificationToQueue(notif, this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /**
     * Fetches public profiles from internal memory.
     *
     * @return public profiles (if any)
     */
    public static ArrayList<Profile> getPublicProfiles(Activity act) {
        ArrayList<Profile> data = null;
        try {
            File file = new File(act.getFilesDir() + "/public.dat");
            FileInputStream in = new FileInputStream(file);
            ObjectInputStream objin = new ObjectInputStream(in);
            data = (ArrayList<Profile>) objin.readObject();
            objin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Saves public profiles.
     *
     * @param data public profiles to be saved
     */
    public static void savePublicProfiles(Activity act, List<Profile> data) {
        try {
            File file = new File(act.getFilesDir() + "/public.dat");
            FileOutputStream out = new FileOutputStream(file);
            ObjectOutputStream objout = new ObjectOutputStream(out);
            objout.writeObject(data);
            objout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Write all notes to internal memory
     *
     * @param activity
     * @param notes
     */
    public static void writeAllNotes(Activity activity, List<Notification> notes) {
        try {
            File file = new File(activity.getFilesDir() + "/notes.dat");
            FileOutputStream out = new FileOutputStream(file);
            ObjectOutputStream objout = new ObjectOutputStream(out);
            objout.writeObject(notes);
            objout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Read all public notes in system.
     *
     * @param activity
     * @return
     */
    public static List<Notification> readAllNotes(Activity activity) {
        if (notifications == null) {
            try {
                File file = new File(activity.getFilesDir() + "/notes.dat");
                FileInputStream in = new FileInputStream(file);
                ObjectInputStream objin = new ObjectInputStream(in);
                notifications = (ArrayList<Notification>) objin.readObject();
                objin.close();
            } catch (Exception e) {
                e.printStackTrace();
                notifications = new ArrayList<>();
            }
        }
        return notifications;
    }
}
