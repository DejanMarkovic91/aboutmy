package rs.hump.aboutmy.database;

import android.content.Context;

import com.orm.Database;
import com.orm.SugarDb;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * Manually inject key for database
 *
 * @author Dejan Markovic
 */
public class EncryptedDatabase extends Database{

    private final String databasePassword;
    private SugarDb sugarDb;
    private SQLiteDatabase sqLiteDatabase;

    public EncryptedDatabase(Context var1, String databasePassword) {
        super(var1);
        SQLiteDatabase.loadLibs(var1);
        this.databasePassword = databasePassword;
        this.sugarDb = new SugarDb(var1);
    }

    @Override
    public synchronized SQLiteDatabase getDB() {
        if(this.sqLiteDatabase == null) {
            this.sqLiteDatabase = this.sugarDb.getWritableDatabase(this.databasePassword);
        }

        return this.sqLiteDatabase;
    }
}
