package rs.hump.aboutmy.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

/**
 * Notification that is not being encrypted in system, and is just handled to show notification in bar.
 *
 * @author Dejan Markovic
 */
public class AbstractNotification implements Parcelable, Serializable {

    private long notificationId;
    private long due;
    private int privateProfile;

    public AbstractNotification() {
    }

    protected AbstractNotification(Parcel in) {
        notificationId = in.readLong();
        due = in.readLong();
        privateProfile = in.readInt();
    }

    public static final Creator<AbstractNotification> CREATOR = new Creator<AbstractNotification>() {
        @Override
        public AbstractNotification createFromParcel(Parcel in) {
            return new AbstractNotification(in);
        }

        @Override
        public AbstractNotification[] newArray(int size) {
            return new AbstractNotification[size];
        }
    };

    public long getDue() {
        return due;
    }

    public void setDue(long due) {
        this.due = due;
    }

    public long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }

    public int getPrivateProfile() {
        return privateProfile;
    }

    public void setPrivateProfile(int privateProfile) {
        this.privateProfile = privateProfile;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(notificationId);
        dest.writeLong(due);
        dest.writeInt(privateProfile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractNotification that = (AbstractNotification) o;

        return notificationId == that.notificationId;

    }

    @Override
    public int hashCode() {
        return (int) (notificationId ^ (notificationId >>> 32));
    }

    @Override
    public String toString() {
        return "AbstractNotification{" +
                "notificationId=" + notificationId +
                ", due=" + due +
                ", privateProfile=" + privateProfile +
                '}';
    }
}
