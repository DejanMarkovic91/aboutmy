package rs.hump.aboutmy.domain;

import android.os.Parcel;
import android.os.Parcelable;
import com.orm.SugarRecord;

import rs.hump.aboutmy.domain.attributes.TipMessage;

/**
 * Notification representation in system.
 *
 * @author Dejan Markovic
 */
public class Notification extends SugarRecord<Notification> implements Parcelable, ProfileItem {

    long length;
    String message;
    long dueTime;
    long period;
    Profile profile;//cant be null
    TipMessage tip;
    long attributeId;

    public Notification() {
    }

    public Notification(Parcel in) {
        id = in.readLong();
        message = in.readString();
        dueTime = in.readLong();
        length = in.readLong();
        period = in.readLong();
        profile = (Profile) in.readValue(Profile.class.getClassLoader());
        tip = (TipMessage) in.readValue(TipMessage.class.getClassLoader());
        attributeId = in.readLong();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(long dueTime) {
        this.dueTime = dueTime;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getPeriod() {
        return period;
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    public TipMessage getTip() {
        return tip;
    }

    public void setTip(TipMessage tip) {
        this.tip = tip;
    }

    public long getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(long attributeId) {
        this.attributeId = attributeId;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(message);
        dest.writeLong(dueTime);
        dest.writeLong(length);
        dest.writeLong(period);
        dest.writeValue(profile);
        dest.writeValue(tip);
        dest.writeLong(attributeId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    /*public void scheduleNextNotification(Context context, String message) {
        AbstractNotification abstractNotification = new AbstractNotification();
        Calendar now = Calendar.getInstance();
        long launchTime = now.getTimeInMillis() + period;
        abstractNotification.setDue(launchTime - Math.abs(showTime - dueTime)); // day earlier
        Notification notification = new Notification();
        notification.setDueTime(launchTime - Math.abs(showTime - dueTime));
        notification.setMessage(message);
        notification.setProfile(profile);
        notification.setLength(length);
        notification.save();
        profile.getNotifications().add(notification);
        abstractNotification.setNotificationId(notification.getId());
        MainActivity.addNotificationToQueue(abstractNotification, context);
    }*/
}
