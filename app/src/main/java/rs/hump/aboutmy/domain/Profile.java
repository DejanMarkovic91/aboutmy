package rs.hump.aboutmy.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rs.hump.aboutmy.domain.attributes.Birthday;
import rs.hump.aboutmy.domain.attributes.FavouriteBook;
import rs.hump.aboutmy.domain.attributes.FavouriteColor;
import rs.hump.aboutmy.domain.attributes.FavouriteDrink;
import rs.hump.aboutmy.domain.attributes.FavouriteFlowers;
import rs.hump.aboutmy.domain.attributes.FavouriteFood;
import rs.hump.aboutmy.domain.attributes.FavouriteMusic;
import rs.hump.aboutmy.domain.attributes.Height;
import rs.hump.aboutmy.domain.attributes.MenstrualCalendar;
import rs.hump.aboutmy.domain.attributes.ShirtSize;
import rs.hump.aboutmy.domain.attributes.ShoeSize;
import rs.hump.aboutmy.domain.attributes.TShirtSize;
import rs.hump.aboutmy.domain.attributes.TrousersSize;
import rs.hump.aboutmy.domain.attributes.WeddingDay;
import rs.hump.aboutmy.domain.attributes.Weight;

/**
 * Representation of single profile in system
 *
 * @author Dejan Markovic
 */
public class Profile extends SugarRecord<Profile> implements Parcelable, Serializable {
    String name;
    String gender;
    String relation;
    String hairColor;
    String eyeColor;
    int privateProfile;
    int globalPIN;
    String PIN;
    Birthday birthday;
    WeddingDay weddingDay;
    Height height;
    Weight weight;
    FavouriteFlowers favouriteFlowers;
    ShoeSize shoeSize;
    ShirtSize shirtSize;
    FavouriteColor favouriteColor;
    MenstrualCalendar menstrualCalendar;
    TrousersSize trousersSize;
    TShirtSize tShirtSize;
    FavouriteBook favouriteBook;
    FavouriteDrink favouriteDrink;
    FavouriteFood favouriteFood;
    FavouriteMusic favouriteMusic;

    @Ignore
    List<Notification> notifications = new ArrayList<>();

    public Profile() {

    }

    protected Profile(Parcel in) {
        id = in.readLong();
        name = in.readString();
        gender = in.readString();
        relation = in.readString();
        hairColor = in.readString();
        eyeColor = in.readString();
        privateProfile = in.readInt();
        globalPIN = in.readInt();
        PIN = in.readString();
        birthday = (Birthday) in.readValue(Birthday.class.getClassLoader());
        weddingDay = (WeddingDay) in.readValue(WeddingDay.class.getClassLoader());
        height = (Height) in.readValue(Height.class.getClassLoader());
        weight = (Weight) in.readValue(Weight.class.getClassLoader());
        favouriteFlowers = (FavouriteFlowers) in.readValue(FavouriteFlowers.class.getClassLoader());
        shoeSize = (ShoeSize) in.readValue(ShoeSize.class.getClassLoader());
        shirtSize = (ShirtSize) in.readValue(ShirtSize.class.getClassLoader());
        favouriteColor = (FavouriteColor) in.readValue(FavouriteColor.class.getClassLoader());
        menstrualCalendar = (MenstrualCalendar) in.readValue(MenstrualCalendar.class.getClassLoader());
        trousersSize = (TrousersSize) in.readValue(TrousersSize.class.getClassLoader());
        tShirtSize = (TShirtSize) in.readValue(TShirtSize.class.getClassLoader());
        favouriteBook = (FavouriteBook) in.readValue(FavouriteBook.class.getClassLoader());
        favouriteDrink = (FavouriteDrink) in.readValue(FavouriteDrink.class.getClassLoader());
        favouriteFood = (FavouriteFood) in.readValue(FavouriteFood.class.getClassLoader());
        favouriteMusic = (FavouriteMusic) in.readValue(FavouriteMusic.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id != null)
            dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(gender);
        dest.writeString(relation);
        dest.writeString(hairColor);
        dest.writeString(eyeColor);
        dest.writeInt(privateProfile);
        dest.writeInt(globalPIN);
        dest.writeString(PIN);
        dest.writeValue(birthday);
        dest.writeValue(weddingDay);
        dest.writeValue(height);
        dest.writeValue(weight);
        dest.writeValue(favouriteFlowers);
        dest.writeValue(shoeSize);
        dest.writeValue(shirtSize);
        dest.writeValue(favouriteColor);
        dest.writeValue(menstrualCalendar);
        dest.writeValue(trousersSize);
        dest.writeValue(tShirtSize);
        dest.writeValue(favouriteBook);
        dest.writeValue(favouriteDrink);
        dest.writeValue(favouriteFood);
        dest.writeValue(favouriteMusic);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public int getPrivateProfile() {
        return privateProfile;
    }

    public void setPrivateProfile(int privateProfile) {
        this.privateProfile = privateProfile;
    }

    public int getGlobalPIN() {
        return globalPIN;
    }

    public void setGlobalPIN(int globalPIN) {
        this.globalPIN = globalPIN;
    }

    public String getPIN() {
        return PIN;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }

    public Birthday getBirthday() {
        return birthday;
    }

    public void setBirthday(Birthday birthday) {
        this.birthday = birthday;
    }

    public WeddingDay getWeddingDay() {
        return weddingDay;
    }

    public void setWeddingDay(WeddingDay weddingDay) {
        this.weddingDay = weddingDay;
    }

    public Height getHeight() {
        return height;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public FavouriteFlowers getFavouriteFlowers() {
        return favouriteFlowers;
    }

    public void setFavouriteFlowers(FavouriteFlowers favouriteFlowers) {
        this.favouriteFlowers = favouriteFlowers;
    }

    public ShoeSize getShoeSize() {
        return shoeSize;
    }

    public void setShoeSize(ShoeSize shoeSize) {
        this.shoeSize = shoeSize;
    }

    public ShirtSize getShirtSize() {
        return shirtSize;
    }

    public void setShirtSize(ShirtSize shirtSize) {
        this.shirtSize = shirtSize;
    }

    public FavouriteColor getFavouriteColor() {
        return favouriteColor;
    }

    public void setFavouriteColor(FavouriteColor favouriteColor) {
        this.favouriteColor = favouriteColor;
    }

    public MenstrualCalendar getMenstrualCalendar() {
        return menstrualCalendar;
    }

    public void setMenstrualCalendar(MenstrualCalendar menstrualCalendar) {
        this.menstrualCalendar = menstrualCalendar;
    }

    public TrousersSize getTrousersSize() {
        return trousersSize;
    }

    public void setTrousersSize(TrousersSize trousersSize) {
        this.trousersSize = trousersSize;
    }

    public TShirtSize gettShirtSize() {
        return tShirtSize;
    }

    public void settShirtSize(TShirtSize tShirtSize) {
        this.tShirtSize = tShirtSize;
    }

    public FavouriteBook getFavouriteBook() {
        return favouriteBook;
    }

    public void setFavouriteBook(FavouriteBook favouriteBook) {
        this.favouriteBook = favouriteBook;
    }

    public FavouriteDrink getFavouriteDrink() {
        return favouriteDrink;
    }

    public void setFavouriteDrink(FavouriteDrink favouriteDrink) {
        this.favouriteDrink = favouriteDrink;
    }

    public FavouriteFood getFavouriteFood() {
        return favouriteFood;
    }

    public void setFavouriteFood(FavouriteFood favouriteFood) {
        this.favouriteFood = favouriteFood;
    }

    public FavouriteMusic getFavouriteMusic() {
        return favouriteMusic;
    }

    public void setFavouriteMusic(FavouriteMusic favouriteMusic) {
        this.favouriteMusic = favouriteMusic;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        return !(id != null ? !id.equals(profile.id) : profile.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", relation='" + relation + '\'' +
                ", hairColor='" + hairColor + '\'' +
                ", eyeColor='" + eyeColor + '\'' +
                ", privateProfile=" + privateProfile +
                ", globalPIN=" + globalPIN +
                ", PIN='" + PIN + '\'' +
                ", birthday=" + birthday +
                ", weddingDay=" + weddingDay +
                ", height=" + height +
                ", weight=" + weight +
                ", favouriteFlowers=" + favouriteFlowers +
                ", shoeSize=" + shoeSize +
                ", shirtSize=" + shirtSize +
                ", favouriteColor=" + favouriteColor +
                ", menstrualCalendar=" + menstrualCalendar +
                ", trousersSize=" + trousersSize +
                ", tShirtSize=" + tShirtSize +
                ", favouriteBook=" + favouriteBook +
                ", favouriteDrink=" + favouriteDrink +
                ", favouriteFood=" + favouriteFood +
                ", favouriteMusic=" + favouriteMusic +
                ", notifications=" + notifications +
                '}';
    }

    public Profile copyProfile() {
        Profile profile = new Profile();
        profile.setId(id);
        profile.setName(name);
        profile.setGender(gender);
        profile.setRelation(relation);
        profile.setHairColor(hairColor);
        profile.setEyeColor(eyeColor);
        profile.setPrivateProfile(privateProfile);
        profile.setGlobalPIN(globalPIN);
        profile.setPIN(PIN);
        profile.setBirthday(birthday);
        profile.setWeddingDay(weddingDay);
        profile.setHeight(height);
        profile.setWeight(weight);
        profile.setFavouriteFlowers(favouriteFlowers);
        profile.setShoeSize(shoeSize);
        profile.setShirtSize(shirtSize);
        profile.setFavouriteColor(favouriteColor);
        profile.setMenstrualCalendar(menstrualCalendar);
        profile.setTrousersSize(trousersSize);
        profile.settShirtSize(tShirtSize);
        profile.setFavouriteBook(favouriteBook);
        profile.setFavouriteDrink(favouriteDrink);
        profile.setFavouriteFood(favouriteFood);
        profile.setFavouriteMusic(favouriteMusic);
        profile.setNotifications(notifications);
        return profile;
    }

    public boolean containsAttribute(long attributeId) {
        if (attributeId == 0) return true;
        if (birthday != null && attributeId == birthday.getId()) {
            return true;
        }
        if (weddingDay != null && attributeId == weddingDay.getId()) {
            return true;
        }
        if (height != null && attributeId == height.getId()) {
            return true;
        }
        if (weight != null && attributeId == weight.getId()) {
            return true;
        }
        if (favouriteFlowers != null && attributeId == favouriteFlowers.getId()) {
            return true;
        }
        if (shoeSize != null && attributeId == shoeSize.getId()) {
            return true;
        }
        if (shirtSize != null && attributeId == shirtSize.getId()) {
            return true;
        }
        if (favouriteColor != null && attributeId == favouriteColor.getId()) {
            return true;
        }
        if (menstrualCalendar != null && attributeId == menstrualCalendar.getId()) {
            return true;
        }
        if (trousersSize != null && attributeId == trousersSize.getId()) {
            return true;
        }
        if (tShirtSize != null && attributeId == tShirtSize.getId()) {
            return true;
        }
        if (favouriteBook != null && attributeId == favouriteBook.getId()) {
            return true;
        }
        if (favouriteDrink != null && attributeId == favouriteDrink.getId()) {
            return true;
        }
        if (favouriteFood != null && attributeId == favouriteFood.getId()) {
            return true;
        }
        if (favouriteMusic != null && attributeId == favouriteMusic.getId()) {
            return true;
        }
        return false;
    }
}
