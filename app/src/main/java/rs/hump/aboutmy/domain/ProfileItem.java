package rs.hump.aboutmy.domain;

import java.io.Serializable;

/**
 * United item for attribute and notifications.
 * Heavy budzevina da se napravi da se razlicita dva objekta prikazuju u istoj listi.
 *
 * @author Dejan Markovic
 */
public interface ProfileItem extends Serializable {

}
