package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.io.Serializable;

import rs.hump.aboutmy.RoboAppCompatActivity;
import rs.hump.aboutmy.domain.ProfileItem;

/**
 * @author Dejan Markovic
 */
public interface Attribute extends ProfileItem {
    int getMaxNumberShowing();

    int getPriority();

    Drawable getAttributeIconResource(Context context);

    String getTextualRepresentation();

    void showDialogWithSummaryInfo(RoboAppCompatActivity activity);
}
