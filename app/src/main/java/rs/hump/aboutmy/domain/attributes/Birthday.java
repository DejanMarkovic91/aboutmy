package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import java.util.Calendar;
import java.util.Date;

import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class Birthday extends SugarRecord<Birthday> implements Parcelable, Attribute {

    long birthday;

    public Birthday() {
    }

    public Birthday(Parcel in) {
        id = in.readLong();
        birthday = in.readLong();
    }

    public static final Creator<Birthday> CREATOR = new Creator<Birthday>() {
        @Override
        public Birthday createFromParcel(Parcel in) {
            return new Birthday(in);
        }

        @Override
        public Birthday[] newArray(int size) {
            return new Birthday[size];
        }
    };

    public long getBirthday() {
        return birthday;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id != null) dest.writeLong(id);
        dest.writeLong(birthday);
    }

    @Override
    public String toString() {
        return "Birthday{" +
                "birthday=" + birthday +
                "} ";
    }

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 7;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_birthday);
    }

    @Override
    public String getTextualRepresentation() {
        return Constants.dateFormat.format(birthday);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        Calendar now = Calendar.getInstance();
        Calendar bd = Calendar.getInstance();
        bd.setTimeInMillis(birthday);
        while(bd.get(Calendar.YEAR) < now.get(Calendar.YEAR)){
            bd.add(Calendar.YEAR, 1);
        }
        long diff = bd.getTimeInMillis() - now.getTimeInMillis();
        diff /= Constants.DAY;
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.birthday_in_time, diff))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
