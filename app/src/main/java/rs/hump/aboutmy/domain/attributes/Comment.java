package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class Comment extends SugarRecord<Comment> implements Parcelable, Attribute {

    private String comment;

    public Comment() {
    }

    protected Comment(Parcel in) {
        id = in.readLong();
        comment = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int getMaxNumberShowing() {
        return 10;
    }

    @Override
    public int getPriority() {
        return 8;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_comment);
    }

    @Override
    public String getTextualRepresentation() {
        return comment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(comment);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(comment)
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
