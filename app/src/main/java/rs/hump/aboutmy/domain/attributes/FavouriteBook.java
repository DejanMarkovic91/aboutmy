package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class FavouriteBook extends SugarRecord<FavouriteBook> implements Parcelable, Attribute {

    private String bookName;

    public FavouriteBook() {
    }

    protected FavouriteBook(Parcel in) {
        id = in.readLong();
        bookName = in.readString();
    }

    public static final Creator<FavouriteBook> CREATOR = new Creator<FavouriteBook>() {
        @Override
        public FavouriteBook createFromParcel(Parcel in) {
            return new FavouriteBook(in);
        }

        @Override
        public FavouriteBook[] newArray(int size) {
            return new FavouriteBook[size];
        }
    };

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public int getMaxNumberShowing() {
        return 4;
    }

    @Override
    public int getPriority() {
        return 6;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_favourite_book);
    }

    @Override
    public String getTextualRepresentation() {
        return bookName;
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.favourite_book_description, bookName))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(bookName);
    }
}
