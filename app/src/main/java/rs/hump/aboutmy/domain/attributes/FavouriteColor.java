package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class FavouriteColor extends SugarRecord<FavouriteColor> implements Parcelable, Attribute {

    private int color = -1;

    public FavouriteColor() {
    }

    protected FavouriteColor(Parcel in) {
        id = in.readLong();
        color = in.readInt();
    }

    public static final Creator<FavouriteColor> CREATOR = new Creator<FavouriteColor>() {
        @Override
        public FavouriteColor createFromParcel(Parcel in) {
            return new FavouriteColor(in);
        }

        @Override
        public FavouriteColor[] newArray(int size) {
            return new FavouriteColor[size];
        }
    };

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int getMaxNumberShowing() {
        return 2;
    }

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        if(color == -1)return ContextCompat.getDrawable(context, R.drawable.ic_fav_color);
        return new ColorDrawable(color);
    }

    @Override
    public String getTextualRepresentation() {
        return "Favourite color";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(color);
    }


    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.favourite_color_description))
                .titleColor(color)
                .buttonRippleColor(color)
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
