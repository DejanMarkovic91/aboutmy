package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class FavouriteDrink extends SugarRecord<FavouriteDrink> implements Attribute, Parcelable {

    private String drink;

    public FavouriteDrink() {
    }

    protected FavouriteDrink(Parcel in) {
        id = in.readLong();
        drink = in.readString();
    }

    public static final Creator<FavouriteDrink> CREATOR = new Creator<FavouriteDrink>() {
        @Override
        public FavouriteDrink createFromParcel(Parcel in) {
            return new FavouriteDrink(in);
        }

        @Override
        public FavouriteDrink[] newArray(int size) {
            return new FavouriteDrink[size];
        }
    };

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Override
    public int getMaxNumberShowing() {
        return 2;
    }

    @Override
    public int getPriority() {
        return 5;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_favourite_drink);
    }

    @Override
    public String getTextualRepresentation() {
        return drink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(drink);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.favourite_drink_description, drink))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
