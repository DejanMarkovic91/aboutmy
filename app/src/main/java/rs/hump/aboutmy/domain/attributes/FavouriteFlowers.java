package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class FavouriteFlowers extends SugarRecord<FavouriteFlowers> implements Parcelable, Attribute {

    private String flowerName;

    public FavouriteFlowers() {
    }

    protected FavouriteFlowers(Parcel in) {
        id = in.readLong();
        flowerName = in.readString();
    }

    public static final Creator<FavouriteFlowers> CREATOR = new Creator<FavouriteFlowers>() {
        @Override
        public FavouriteFlowers createFromParcel(Parcel in) {
            return new FavouriteFlowers(in);
        }

        @Override
        public FavouriteFlowers[] newArray(int size) {
            return new FavouriteFlowers[size];
        }
    };

    public String getFlowerName() {
        return flowerName;
    }

    public void setFlowerName(String flowerName) {
        this.flowerName = flowerName;
    }

    @Override
    public int getMaxNumberShowing() {
        return 2;
    }

    @Override
    public int getPriority() {
        return 3;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_flower);
    }

    @Override
    public String getTextualRepresentation() {
        return flowerName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(flowerName);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.favourite_flower_name, flowerName))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
