package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class FavouriteFood extends SugarRecord<FavouriteFood> implements Parcelable, Attribute{

    private String foodName;

    public FavouriteFood() {
    }

    protected FavouriteFood(Parcel in) {
        foodName = in.readString();
    }

    public static final Creator<FavouriteFood> CREATOR = new Creator<FavouriteFood>() {
        @Override
        public FavouriteFood createFromParcel(Parcel in) {
            return new FavouriteFood(in);
        }

        @Override
        public FavouriteFood[] newArray(int size) {
            return new FavouriteFood[size];
        }
    };

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    @Override
    public int getMaxNumberShowing() {
        return 2;
    }

    @Override
    public int getPriority() {
        return 4;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_favourite_food);
    }

    @Override
    public String getTextualRepresentation() {
        return foodName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(foodName);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.favourite_food_description, foodName))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
