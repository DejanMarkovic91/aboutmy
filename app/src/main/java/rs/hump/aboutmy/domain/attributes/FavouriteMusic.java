package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class FavouriteMusic extends SugarRecord<FavouriteMusic> implements Parcelable, Attribute {

    private String name;

    public FavouriteMusic() {
    }

    protected FavouriteMusic(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }

    public static final Creator<FavouriteMusic> CREATOR = new Creator<FavouriteMusic>() {
        @Override
        public FavouriteMusic createFromParcel(Parcel in) {
            return new FavouriteMusic(in);
        }

        @Override
        public FavouriteMusic[] newArray(int size) {
            return new FavouriteMusic[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getMaxNumberShowing() {
        return 4;
    }

    @Override
    public int getPriority() {
        return 7;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_favourite_music);
    }

    @Override
    public String getTextualRepresentation() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.favourite_music_description, name))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
