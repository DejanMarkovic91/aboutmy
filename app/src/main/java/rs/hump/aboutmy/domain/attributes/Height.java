package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import java.util.Calendar;

import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class Height extends SugarRecord<Height> implements Parcelable, Attribute {

    private int height;

    public Height() {

    }

    protected Height(Parcel in) {
        id = in.readLong();
        height = in.readInt();
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 50;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_height);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(height);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Height> CREATOR = new Parcelable.Creator<Height>() {
        @Override
        public Height createFromParcel(Parcel in) {
            return new Height(in);
        }

        @Override
        public Height[] newArray(int size) {
            return new Height[size];
        }
    };

    @Override
    public String getTextualRepresentation() {
        return height + " cm";
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.height_of_person, height))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
