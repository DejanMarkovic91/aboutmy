package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.orm.SugarRecord;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class MenstrualCalendar extends SugarRecord<MenstrualCalendar> implements Parcelable, Attribute {

    private static final String TAG = "CALDROID";
    private long start;
    private int avgLengthOfPeriod;

    public MenstrualCalendar() {
    }

    protected MenstrualCalendar(Parcel in) {
        id = in.readLong();
        start = in.readLong();
        avgLengthOfPeriod = in.readInt();
    }

    public static final Creator<MenstrualCalendar> CREATOR = new Creator<MenstrualCalendar>() {
        @Override
        public MenstrualCalendar createFromParcel(Parcel in) {
            return new MenstrualCalendar(in);
        }

        @Override
        public MenstrualCalendar[] newArray(int size) {
            return new MenstrualCalendar[size];
        }
    };

    public long getStart() {
        return start;
    }

    public void setStart(long st) {
        this.start = st;
    }

    public int getAvgLengthOfPeriod() {
        return avgLengthOfPeriod;
    }

    public void setAvgLengthOfPeriod(int avgLengthOfPeriod) {
        this.avgLengthOfPeriod = avgLengthOfPeriod;
    }

    public Date getOvulationDate() {
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(this.start);
        start.add(Calendar.DATE, avgLengthOfPeriod / 2);
        return start.getTime();
    }

    public void getFertileDates(HashMap<Date, Integer> result) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getOvulationDate());
        cal.add(Calendar.DATE, -3);
        for (int i = 0; i < 7; i++) {
            result.put(cal.getTime(), R.drawable.ic_fertile);
            cal.add(Calendar.DATE, 1);
        }
        cal.add(Calendar.DATE, getAvgLengthOfPeriod());
        cal.add(Calendar.DATE, -3);
        for (int i = 0; i < 7; i++) {
            result.put(cal.getTime(), R.drawable.ic_fertile);
            cal.add(Calendar.DATE, 1);
        }
    }


    private void getBloodyDays(HashMap<Date, Integer> bloody) {
        Calendar periodStart = Calendar.getInstance();
        periodStart.setTimeInMillis(start);
        int length = avgLengthOfPeriod < 28 ? 4 : 5;
        for (int i = 0; i < length; i++) {
            bloody.put(periodStart.getTime(), R.drawable.ic_blood);
            periodStart.add(Calendar.DATE, 1);
        }
        periodStart.add(Calendar.DATE, avgLengthOfPeriod - length);
        for (int i = 0; i < length; i++) {
            bloody.put(periodStart.getTime(), R.drawable.ic_blood);
            periodStart.add(Calendar.DATE, 1);
        }
    }


    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 6;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_menstrual_calendar);
    }

    @Override
    public String getTextualRepresentation() {
        return "Menstrual calendar";
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        Calendar now = Calendar.getInstance();
        CaldroidFragment dialogCaldroidFragment = CaldroidFragment.newInstance(
                activity.getString(R.string.menstrual_calendar), now.get(Calendar.MONTH), now.get(Calendar.YEAR));
        Bundle args = new Bundle();
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
        args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
        args.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, false);
        now.add(Calendar.MONTH, -1);
        dialogCaldroidFragment.setMinDate(now.getTime());
        dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                //TODO show detailed information
            }
        });
        HashMap<Date, Integer> dates = new HashMap<>();
        getBloodyDays(dates);
        getFertileDates(dates);
        //dialogCaldroidFragment.setBackgroundResourceForDates(dates);
        Date ovulDate = getOvulationDate();
        //dialogCaldroidFragment.setBackgroundResourceForDate(R.drawable.ic_ovulation, ovulDate);
        Calendar temp = Calendar.getInstance();
        temp.setTime(ovulDate);
        temp.add(Calendar.DATE, getAvgLengthOfPeriod() + 4);
        ovulDate = temp.getTime();
        //dialogCaldroidFragment.setBackgroundResourceForDate(R.drawable.ic_ovulation, ovulDate);
        dialogCaldroidFragment.show(activity.getSupportFragmentManager(), TAG);
    }

    public int getCurrentDayResource() {
        int res = R.drawable.ic_menstrual_calendar;
        Calendar st = Calendar.getInstance();
        st.setTimeInMillis(start);
        Calendar now = Calendar.getInstance();
        while (now.getTimeInMillis() - st.getTimeInMillis() > Constants.DAY * avgLengthOfPeriod) {
            st.add(Calendar.DATE, avgLengthOfPeriod);
        }

        //now we have current month
        int bloody = avgLengthOfPeriod < 28 ? 4 : 5;
        Calendar lastBloody = Calendar.getInstance();
        lastBloody.setTimeInMillis(st.getTimeInMillis());
        lastBloody.add(Calendar.DATE, bloody);

        if (now.getTimeInMillis() < lastBloody.getTimeInMillis()) {
            //bloody period
            res = R.drawable.ic_blood;
            return res;
        }

        Calendar ovul = Calendar.getInstance();
        ovul.setTimeInMillis(this.start);
        ovul.add(Calendar.DATE, avgLengthOfPeriod / 2);

        Calendar threeDaysBefore = Calendar.getInstance();
        threeDaysBefore.setTimeInMillis(ovul.getTimeInMillis() - 3 * Constants.DAY);
        Calendar threeDaysAfter = Calendar.getInstance();
        threeDaysAfter.setTimeInMillis(ovul.getTimeInMillis() + 3 * Constants.DAY);

        if (threeDaysBefore.getTimeInMillis() < now.getTimeInMillis() &&
                threeDaysAfter.getTimeInMillis() > now.getTimeInMillis()) {
            if (now.get(Calendar.DAY_OF_MONTH) == ovul.get(Calendar.DAY_OF_MONTH)) {
                res = R.drawable.ic_ovulation;
            } else {
                res = R.drawable.ic_fertile;
            }
            return res;
        }

        return res;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(start);
        dest.writeInt(avgLengthOfPeriod);
    }
}
