package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * Custom note for user in system.
 *
 * @author Dejan Markovic
 */
public class Note extends SugarRecord<Note> implements Attribute, Parcelable {

    private String text;

    public Note() {
    }

    protected Note(Parcel in) {
        id = in.readLong();
        text = in.readString();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_note);
    }

    @Override
    public String getTextualRepresentation() {
        return text;
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(text);
    }
}
