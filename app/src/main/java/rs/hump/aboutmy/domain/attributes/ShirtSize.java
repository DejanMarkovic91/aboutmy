package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class ShirtSize extends SugarRecord<ShirtSize> implements Parcelable, Attribute {

    public static String[] shirtSizes = new String[]{"XS", "S", "M", "L", "XL", "XXL"};
    private String size;
    private String origin = "EU";//optional

    public ShirtSize() {
    }

    protected ShirtSize(Parcel in) {
        id = in.readLong();
        size = in.readString();
        origin = in.readString();
    }

    public static final Creator<ShirtSize> CREATOR = new Creator<ShirtSize>() {
        @Override
        public ShirtSize createFromParcel(Parcel in) {
            return new ShirtSize(in);
        }

        @Override
        public ShirtSize[] newArray(int size) {
            return new ShirtSize[size];
        }
    };

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 22;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_shirt);
    }

    @Override
    public String getTextualRepresentation() {
        return String.format("%s %s", size, origin != null ? "(" + origin + ")" : "");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(size);
        dest.writeString(origin);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.shirt_size_description, size, origin))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }

}
