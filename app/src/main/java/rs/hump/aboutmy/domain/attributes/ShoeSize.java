package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class ShoeSize extends SugarRecord<ShoeSize> implements Parcelable, Attribute {

    private int size;
    private int subSize;//optional
    private String origin = "EU";

    public ShoeSize() {
    }

    protected ShoeSize(Parcel in) {
        id = in.readLong();
        size = in.readInt();
        subSize = in.readInt();
        origin = in.readString();
    }

    public static final Creator<ShoeSize> CREATOR = new Creator<ShoeSize>() {
        @Override
        public ShoeSize createFromParcel(Parcel in) {
            return new ShoeSize(in);
        }

        @Override
        public ShoeSize[] newArray(int size) {
            return new ShoeSize[size];
        }
    };

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSubSize() {
        return subSize;
    }

    public void setSubSize(int subSize) {
        this.subSize = subSize;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 21;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_shoe);
    }

    @Override
    public String getTextualRepresentation() {
        return String.format("%d%s %s", size, subSize != 0 ? "," + subSize : "", "(" + origin + ")");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(size);
        dest.writeInt(subSize);
        dest.writeString(origin);
    }

    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.shoe_size_description, size, subSize, origin))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
