package rs.hump.aboutmy.domain.attributes;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;

/**
 * Wisdom to be shown for single notification.
 *
 * @author Dejan Markovic
 */
public class TipMessage extends SugarRecord<TipMessage> implements Parcelable {

    private String message;
    private String relation;
    private String gender;

    public TipMessage() {

    }

    protected TipMessage(Parcel in) {
        id = in.readLong();
        message = in.readString();
        relation = in.readString();
        gender = in.readString();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static final Creator<TipMessage> CREATOR = new Creator<TipMessage>() {
        @Override
        public TipMessage createFromParcel(Parcel in) {
            return new TipMessage(in);
        }

        @Override
        public TipMessage[] newArray(int size) {
            return new TipMessage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(message);
        dest.writeString(relation);
        dest.writeString(gender);
    }

    /**
     * Get random tip message for psecified profile and add it to notification
     *
     * @param p Profile for which we are adding tip
     * @param n Notification for which we are adding tip message
     * @return Tip message
     * <p/>
     * TODO see if this can be exported to public part
     */
    public static void findTipMessageAndPutInNotification(Profile p, Notification n) {
        if (p == null || n == null) return;
        TipMessage message = null;
        List<TipMessage> messages = TipMessage.listAll(TipMessage.class), possibleMessages = new ArrayList<>();
        for (TipMessage m : messages) {
            if (m.getGender().equals(p.getGender()) && m.getRelation().equals(p.getRelation())) {
                possibleMessages.add(m);
            }
        }
        Log.i(TipMessage.class.getName(), String.format("Size of possible messages: %d", possibleMessages.size()));
        //get random possible message
        if (!possibleMessages.isEmpty()) {
            int index = new Random().nextInt(possibleMessages.size());
            message = possibleMessages.get(index);
        }
        n.setTip(message);
    }
}
