package rs.hump.aboutmy.domain.attributes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Markovic
 */
public class TrousersSize extends SugarRecord<TrousersSize> implements Parcelable, Attribute {

    private int size;
    private int subSize;
    private String origin = "EU";

    public TrousersSize() {
    }

    protected TrousersSize(Parcel in) {
        id = in.readLong();
        size = in.readInt();
        subSize = in.readInt();
        origin = in.readString();
    }

    public static final Creator<TrousersSize> CREATOR = new Creator<TrousersSize>() {
        @Override
        public TrousersSize createFromParcel(Parcel in) {
            return new TrousersSize(in);
        }

        @Override
        public TrousersSize[] newArray(int size) {
            return new TrousersSize[size];
        }
    };

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSubSize() {
        return subSize;
    }

    public void setSubSize(int subSize) {
        this.subSize = subSize;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 24;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_trousers);
    }

    @Override
    public String getTextualRepresentation() {
        return String.format("%d%s (%s)", size, subSize != 0 ? "," + subSize : "", origin);
    }


    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.trousers_size_description, size, origin))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(size);
        dest.writeInt(subSize);
        dest.writeString(origin);
    }
}
