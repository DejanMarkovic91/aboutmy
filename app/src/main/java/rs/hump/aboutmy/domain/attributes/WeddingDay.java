package rs.hump.aboutmy.domain.attributes;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.SugarRecord;

import java.util.Calendar;
import java.util.Date;

import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RoboAppCompatActivity;

/**
 * @author Dejan Makrkovic
 */
public class WeddingDay extends SugarRecord<WeddingDay> implements Parcelable, Attribute {

    long weddingDay;

    public WeddingDay() {
    }

    public WeddingDay(Parcel in) {
        id = in.readLong();
        weddingDay = in.readLong();
    }

    public static final Creator<WeddingDay> CREATOR = new Creator<WeddingDay>() {
        @Override
        public WeddingDay createFromParcel(Parcel in) {
            return new WeddingDay(in);
        }

        @Override
        public WeddingDay[] newArray(int size) {
            return new WeddingDay[size];
        }
    };

    public long getWeddingDay() {
        return weddingDay;
    }

    public void setWeddingDay(long birthday) {
        this.weddingDay = birthday;
    }

    @Override
    public String toString() {
        return "WeddingDay{" +
                "weddingDay=" + weddingDay +
                "} " + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(weddingDay);
    }

    @Override
    public int getMaxNumberShowing() {
        return 1;
    }

    @Override
    public int getPriority() {
        return 20;
    }

    @Override
    public Drawable getAttributeIconResource(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_wedding);
    }

    @Override
    public String getTextualRepresentation() {
        return Constants.dateFormat.format(weddingDay);
    }


    @Override
    public void showDialogWithSummaryInfo(RoboAppCompatActivity activity) {
        Calendar now = Calendar.getInstance();
        Calendar wd = Calendar.getInstance();
        wd.setTimeInMillis(weddingDay);
        while(wd.get(Calendar.YEAR) < now.get(Calendar.YEAR)){
            wd.add(Calendar.YEAR, 1);
        }
        long diff = wd.getTimeInMillis() - now.getTimeInMillis();
        diff /= Constants.DAY;
        new MaterialDialog.Builder(activity)
                .content(activity.getString(R.string.wedding_in_time, diff))
                .icon(getAttributeIconResource(activity))
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .autoDismiss(true)
                .show();
    }
}
