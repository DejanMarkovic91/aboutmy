package rs.hump.aboutmy.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RefreshableActivity;
import rs.hump.aboutmy.domain.AbstractNotification;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.profile.MainActivity;

/**
 * Handles intents and fires notifications if necessary. It also reschedules those notifications.
 *
 * @author Dejan Markovic
 */
public class NotificationTimeReceiver extends BroadcastReceiver {

    public static final String PENDING_NOTIFICATIONS_FILE = "pending_notif.bin";
    private static final Object synch = new Object();

    @Override
    public void onReceive(Context context, Intent intent) {
        ArrayList<Long> notificationsToBeRescheduled = new ArrayList<>();
        if (intent.getAction().equals(MainActivity.NOTIFICATION)) {
            //notification comes to be shown without restart
            Log.i(NotificationTimeReceiver.class.getName(), "Notification triggered: " + intent.toString());
            Calendar now = Calendar.getInstance();
            ArrayList<AbstractNotification> notifications = MainActivity.readFromInternalStorage(context);
            Iterator<AbstractNotification> iterator = notifications.iterator();
            boolean removedSomething = false;
            while (iterator.hasNext()) {
                AbstractNotification notif = iterator.next();
                Calendar notifDueDate = Calendar.getInstance();
                notifDueDate.setTimeInMillis(notif.getDue());
                if (notifDueDate.compareTo(now) <= 0) {
                    //show bar notification and remove this notification
                    removedSomething = true;
                    //SummaryNotificationManager.addMessage(notif.getNotificationId(), context.getString(R.string.you_have_pending_notifications));
                    SummaryNotificationManager.notify(context, notif);
                    iterator.remove();
                    notificationsToBeRescheduled.add(notif.getNotificationId());
                }//no rescheduling
            }
            if (removedSomething) MainActivity.saveToInternalStorage(context, notifications);
        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            //phone rebooted, we must schedule everything again
            Log.i(NotificationTimeReceiver.class.getName(), "Phone rebooted, rescheduling all notifications: " + intent.toString());

            Calendar now = Calendar.getInstance();
            ArrayList<AbstractNotification> notifications = MainActivity.readFromInternalStorage(context);
            Iterator<AbstractNotification> iterator = notifications.iterator();
            boolean removedSomething = false;
            while (iterator.hasNext()) {
                AbstractNotification notif = iterator.next();
                Calendar notifDueDate = Calendar.getInstance();
                notifDueDate.setTimeInMillis(notif.getDue());
                if (notifDueDate.compareTo(now) <= 0) {//show notification
                    //show bar notification and remove this notification
                    removedSomething = true;
                    //SummaryNotificationManager.addMessage(notif.getNotificationId(), context.getString(R.string.you_have_pending_notifications));
                    iterator.remove();
                    notificationsToBeRescheduled.add(notif.getNotificationId());
                } else {//reschedule notification
                    MainActivity.addNotificationToQueue(notif, context);
                }
            }
            if (removedSomething) MainActivity.saveToInternalStorage(context, notifications);
        }
        //append new notifications to be rescheduled
        if (notificationsToBeRescheduled.size() > 0) {
            Log.i(NotificationTimeReceiver.class.getName(), "Appending notifications to be rescheduled: " + notificationsToBeRescheduled.size());
            updateNotificationList(context, notificationsToBeRescheduled);
        }
    }

    /**
     * Append rescheduled notifications to file which will be scheduled next time activity is run.
     *
     * @param ctx              Context in which files are handled
     * @param newNotifications notifications to be appended
     */
    public static void updateNotificationList(final Context ctx, final ArrayList<Long> newNotifications) {
        new Thread() {
            @Override
            public void run() {
                synchronized (synch) {
                    ArrayList<Long> notifications = null;
                    try {
                        FileInputStream fos = ctx.openFileInput(PENDING_NOTIFICATIONS_FILE);
                        ObjectInputStream in = new ObjectInputStream(fos);
                        notifications = (ArrayList<Long>) in.readObject();
                        in.close();
                    } catch (ClassNotFoundException | IOException e) {
                        Log.e(NotificationTimeReceiver.class.getName(), e.getMessage());
                        notifications = new ArrayList<>();
                    }
                    notifications.addAll(newNotifications);
                    try {
                        FileOutputStream fout = ctx.openFileOutput(PENDING_NOTIFICATIONS_FILE, Context.MODE_PRIVATE);
                        ObjectOutputStream out = new ObjectOutputStream(fout);
                        out.writeObject(notifications);
                        out.flush();
                        out.close();
                    } catch (IOException e) {
                        Log.e(NotificationTimeReceiver.class.getName(), e.getMessage());
                    }
                }
            }
        }.start();
    }

}
