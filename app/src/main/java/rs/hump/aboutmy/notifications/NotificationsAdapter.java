package rs.hump.aboutmy.notifications;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;

/**
 * Adapts notifications view
 *
 * @author Dejan Markovic
 */
public class NotificationsAdapter extends BaseExpandableListAdapter {

    private NotificationsListActivity activity;
    private List<Profile> profiles;

    public NotificationsAdapter(NotificationsListActivity activity) {
        this.activity = activity;
        profiles = activity.getProfilesWithNotifications();
    }

    @Override
    public int getGroupCount() {
        return profiles.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return profiles.get(groupPosition).getNotifications().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return profiles.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return profiles.get(groupPosition).getNotifications().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View recycledView = convertView;
        if (recycledView == null) {
            recycledView = activity.getLayoutInflater().inflate(R.layout.notification_group_item, null);
        }
        ((TextView) recycledView).setText(((Profile) getGroup(groupPosition)).getName());
        return recycledView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View recycledView = convertView;
        if (recycledView == null) {
            recycledView = activity.getLayoutInflater().inflate(R.layout.notification_child_item, null);
        }

        Notification item = (Notification) getChild(groupPosition, childPosition);
        TextView msg = (TextView)recycledView;
        msg.setText(String.format("%s%s", Constants.dateFormat.format(new Date(item.getDueTime())), item.getMessage()));
        return recycledView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
