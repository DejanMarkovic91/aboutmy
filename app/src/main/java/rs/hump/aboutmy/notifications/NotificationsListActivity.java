package rs.hump.aboutmy.notifications;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import roboguice.inject.InjectView;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RefreshableActivity;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.profile.MainActivity;

public class NotificationsListActivity extends RefreshableActivity {

    @InjectView(R.id.toolbar)
    private Toolbar toolbar;
    @InjectView(R.id.notifications_list)
    private ExpandableListView notifications_list;
    private List<Profile> masterProfileList;
    private List<Profile> profiles;
    private NotificationsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        refresh(PIN);
    }

    public List<Profile> getProfilesWithNotifications() {
        return profiles;
    }

    @Override
    public void refresh(String PIN) {
        RefreshableActivity.PIN = PIN;
        profiles = MainActivity.getInstance().getUnlockedProfiles();
        masterProfileList = MainActivity.getInstance().getMasterProfileList();
        adapter = new NotificationsAdapter(this);
        Iterator<Profile> profileIterator = profiles.iterator();
        while (profileIterator.hasNext()) {
            Profile profile = profileIterator.next();
            //fetch notifications
            List<Notification> notes = null;
            if(profile.getPrivateProfile() == 0){
                notes = RefreshableActivity.readAllNotes(this);
                Iterator<Notification> notificationIterator = notes.iterator();
                while(notificationIterator.hasNext()){
                    if(notificationIterator.next().getProfile().getId() != profile.getId())profileIterator.remove();
                }
            }else {
                notes = Notification.findWithQuery(Notification.class, "Select * from Notification where profile = ?", String.valueOf(profile.getId()));
            }
            Iterator<Notification> notesNotificationIterator = notes.iterator();
            long now = Calendar.getInstance().getTimeInMillis();
            while (notesNotificationIterator.hasNext()) {
                Notification item = notesNotificationIterator.next();
                if (item.getDueTime() + item.getLength() < now) {
                    item.delete();//already passed
                    notesNotificationIterator.remove();
                }
                if (!profile.containsAttribute(item.getAttributeId())) {
                    Log.e(NotificationsListActivity.class.getName(), "Removed notification");
                    notesNotificationIterator.remove();
                }

                /*if (item.getDueTime() > now && item.getDueTime() + item.getLength() > now)
                    notesNotificationIterator.remove();//not coming soon*/
            }
            if (notes.isEmpty())
                profileIterator.remove();
            else
                profile.setNotifications(notes);
        }
        //notifications_list.setIndicatorBounds((int) (notifications_list.getRight() -
        //       getResources().getDimension(R.dimen.fields_height)), notifications_list.getWidth());
        notifications_list.setAdapter(adapter);
        for (int i = 0; i < getProfilesWithNotifications().size(); i++) {
            notifications_list.expandGroup(i);
        }
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        //SummaryNotificationManager.ressetMessages(this);

    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.unlock) {
            unlockProfile();
        }
        return super.onOptionsItemSelected(item);
    }

    private void unlockProfile() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.profile_unlock))
                .content(R.string.input_content)
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                .input(R.string.pin, R.string.input_prefill, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        Profile unlocked = null;
                        //is it global PIN ?
                        for (Profile p : masterProfileList) {
                            if (p.getPrivateProfile() == 1 && p.getPIN().equals(input.toString())) {
                                unlocked = p;
                                break;
                            }
                        }
                        if (unlocked != null && !profiles.contains(unlocked)) {
                            profiles.add(unlocked);
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(NotificationsListActivity.this, getString(R.string.error_wrong_pin), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .autoDismiss(false)
                .cancelable(true)
                .negativeText(android.R.string.cancel)
                .show();
    }
}
