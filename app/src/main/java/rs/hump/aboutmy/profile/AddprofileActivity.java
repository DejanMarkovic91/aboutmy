package rs.hump.aboutmy.profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import roboguice.inject.InjectView;
import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RefreshableActivity;
import rs.hump.aboutmy.RoboAppCompatActivity;
import rs.hump.aboutmy.domain.AbstractNotification;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.domain.attributes.Attribute;
import rs.hump.aboutmy.domain.attributes.Birthday;
import rs.hump.aboutmy.domain.attributes.FavouriteBook;
import rs.hump.aboutmy.domain.attributes.FavouriteColor;
import rs.hump.aboutmy.domain.attributes.FavouriteDrink;
import rs.hump.aboutmy.domain.attributes.FavouriteFlowers;
import rs.hump.aboutmy.domain.attributes.FavouriteFood;
import rs.hump.aboutmy.domain.attributes.FavouriteMusic;
import rs.hump.aboutmy.domain.attributes.Height;
import rs.hump.aboutmy.domain.attributes.MenstrualCalendar;
import rs.hump.aboutmy.domain.attributes.ShirtSize;
import rs.hump.aboutmy.domain.attributes.ShoeSize;
import rs.hump.aboutmy.domain.attributes.TShirtSize;
import rs.hump.aboutmy.domain.attributes.TipMessage;
import rs.hump.aboutmy.domain.attributes.TrousersSize;
import rs.hump.aboutmy.domain.attributes.WeddingDay;
import rs.hump.aboutmy.domain.attributes.Weight;
import rs.hump.aboutmy.settings.AppCompatPreferenceActivity;

/**
 * Activity for adding new profile.
 *
 * @author Dejan Markovic
 */
@SuppressWarnings("ALL")
public class AddProfileActivity extends RoboAppCompatActivity {

    private Random randomGenerator = new Random();
    public static final String PROFILE = "rs.hump.aboutmy.profile.PROFILE";

    private Object menstLock = new Object();

    @InjectView(R.id.profile_name)
    private EditText profileName;
    @InjectView(R.id.female)
    private RadioButton female;
    @InjectView(R.id.male)
    private RadioButton male;
    @InjectView(R.id.relation)
    private Spinner relation;
    @InjectView(R.id.hair_color)
    private Spinner hairColor;
    @InjectView(R.id.eye_color)
    private Spinner eyeColor;
    @InjectView(R.id.do_not_show_remainder)
    private CheckBox doNotShowRemainder;
    @InjectView(R.id.private_profile)
    private CheckBox privateProfile;
    @InjectView(R.id.pin_chooser)
    private RadioGroup pinTypeChooser;
    @InjectView(R.id.global_pin)
    private RadioButton globalPin;
    @InjectView(R.id.specific_pin)
    private RadioButton specificPin;
    @InjectView(R.id.specific_pin_holder)
    private EditText pinEditText;
    @InjectView(R.id.attribute_holder)
    private ListView attributeHolder;
    @InjectView(R.id.fab)
    private FloatingActionButton fab;
    @InjectView(R.id.toolbar)
    private Toolbar toolbar;

    private Profile profile = new Profile();
    private Profile oldProfile;
    private static AddProfileActivity instance;
    private boolean dataDirty = false;
    private List<Attribute> attributes = new LinkedList<Attribute>();
    private rs.hump.aboutmy.profile.AttributeAdapter adapter;

    public static AddProfileActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_profile);
        instance = this;
        setSupportActionBar(toolbar);

        //disable scroll of list view
        attributeHolder.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(R.string.choose_attribute)
                        .adapter(
                                new AttributeAdapter(Constants.attributes) {
                                    @Override
                                    public boolean isEnabled(int position) {
                                        Attribute at = (Attribute) getItem(position);
                                        boolean response = true;
                                        if (at instanceof Birthday && profile.getBirthday() != null)
                                            response = false;
                                        if (at instanceof WeddingDay && profile.getWeddingDay() != null)
                                            response = false;
                                        if (at instanceof Height && profile.getHeight() != null)
                                            response = false;
                                        if (at instanceof Weight && profile.getWeight() != null)
                                            response = false;
                                        if (at instanceof FavouriteFlowers && profile.getFavouriteFlowers() != null)
                                            response = false;
                                        if (at instanceof ShoeSize && profile.getShoeSize() != null)
                                            response = false;
                                        if (at instanceof ShirtSize && profile.getShirtSize() != null)
                                            response = false;
                                        if (at instanceof FavouriteColor && profile.getFavouriteColor() != null)
                                            response = false;
                                        if (at instanceof MenstrualCalendar && profile.getMenstrualCalendar() != null)
                                            response = false;
                                        if (at instanceof TrousersSize && profile.getTrousersSize() != null)
                                            response = false;
                                        if (at instanceof TShirtSize && profile.gettShirtSize() != null)
                                            response = false;
                                        if (at instanceof FavouriteBook && profile.getFavouriteBook() != null)
                                            response = false;
                                        if (at instanceof FavouriteDrink && profile.getFavouriteDrink() != null)
                                            response = false;
                                        if (at instanceof FavouriteFood && profile.getFavouriteFood() != null)
                                            response = false;
                                        if (at instanceof FavouriteMusic && profile.getFavouriteMusic() != null)
                                            response = false;
                                        if (!response)
                                            Toast.makeText(AddProfileActivity.this, getString(R.string.error_already_added), Toast.LENGTH_SHORT).show();
                                        switch ((String) relation.getSelectedItem()) {
                                            case "Father":
                                            case "Grandfather":
                                            case "Brother":
                                            case "Lover":
                                            case "Friend":
                                                if (at instanceof MenstrualCalendar) {
                                                    response = false;
                                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_male_person_cant_have_pms), Toast.LENGTH_SHORT).show();
                                                }
                                                break;
                                        }
                                        if ((at instanceof WeddingDay) && !((String) relation.getSelectedItem()).equals("Spouse")) {
                                            response = false;
                                            Toast.makeText(AddProfileActivity.this, getString(R.string.error_not_wedd), Toast.LENGTH_SHORT).show();
                                        }
                                        return response;
                                    }
                                },
                                new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                        handleAttributeClick(which);
                                        dialog.dismiss();
                                    }
                                })
                        .cancelable(true)
                        .show();
            }
        });

        addListeners();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //SummaryNotificationManager.addMessage((long) (Math.random() * 1000), "Yet another notification message!");
        //Dummy notification
        //SummaryNotificationManager.notify(this);
    }

    private void addListeners() {

        relation.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.empty_relations)));

        male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                relation.setAdapter(new ArrayAdapter<String>(AddProfileActivity.this, android.R.layout.simple_spinner_dropdown_item,
                        filterOutByGender(Constants.relations, isChecked)));
                profile.setGender(male.isChecked() ? "male" : "female");
            }
        });
        female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                relation.setAdapter(new ArrayAdapter<String>(AddProfileActivity.this, android.R.layout.simple_spinner_dropdown_item,
                        filterOutByGender(Constants.relations, !isChecked)));
                profile.setGender(male.isChecked() ? "male" : "female");
            }
        });

        hairColor.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.hair_colors)));
        eyeColor.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.eye_colors)));

        privateProfile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    pinTypeChooser.setVisibility(View.VISIBLE);
                else {
                    pinTypeChooser.setVisibility(View.GONE);
                    pinEditText.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Profile tempProfile = intent.getParcelableExtra(PROFILE);
        //refresh view according to profile that we are editing

        if (tempProfile != null) {
            Log.i(AddProfileActivity.class.getName(), "Profile to be edited: " + tempProfile);
            profile = tempProfile;
            profileName.setText(profile.getName());
            switch (profile.getGender()) {
                case "male":
                    male.setChecked(true);
                    break;
                case "female":
                    female.setChecked(true);
                    break;
            }
            relation.setAdapter(new ArrayAdapter<String>(AddProfileActivity.this, android.R.layout.simple_spinner_dropdown_item,
                    filterOutByGender(Constants.relations, male.isChecked())));
            relation.setSelection(findRelationSelectionByName(profile.getRelation()));
            hairColor.setSelection(findHairColorSelectionByName(profile.getHairColor()));
            eyeColor.setSelection(findEyeColorSelectionByName(profile.getEyeColor()));
            privateProfile.setChecked(profile.getPrivateProfile() == 1 ? true : false);
            if (privateProfile.isChecked()) {
                globalPin.setChecked(profile.getGlobalPIN() == 1 ? true : false);
                specificPin.setChecked(!profile.getPIN().isEmpty());
                pinEditText.setText(profile.getPIN());
                if (!profile.getPIN().isEmpty())
                    pinEditText.setVisibility(View.VISIBLE);
            }
            if (profile.getBirthday() != null) {
                addAttribute(profile.getBirthday());
            }
            if (profile.getWeddingDay() != null) {
                addAttribute(profile.getWeddingDay());
            }
            if (profile.getHeight() != null) {
                addAttribute(profile.getHeight());
            }
            if (profile.getWeight() != null) {
                addAttribute(profile.getWeight());
            }
            if (profile.getFavouriteFlowers() != null) {
                addAttribute(profile.getFavouriteFlowers());
            }
            if (profile.getShoeSize() != null) {
                addAttribute(profile.getShoeSize());
            }
            if (profile.getShirtSize() != null) {
                addAttribute(profile.getShirtSize());
            }
            if (profile.getFavouriteColor() != null) {
                addAttribute(profile.getFavouriteColor());
            }
            if (profile.getMenstrualCalendar() != null) {
                addAttribute(profile.getMenstrualCalendar());
            }
            if (profile.getTrousersSize() != null) {
                addAttribute(profile.getTrousersSize());
            }
            if (profile.gettShirtSize() != null) {
                addAttribute(profile.gettShirtSize());
            }
            if (profile.getFavouriteBook() != null) {
                addAttribute(profile.getFavouriteBook());
            }
            if (profile.getFavouriteDrink() != null) {
                addAttribute(profile.getFavouriteDrink());
            }
            if (profile.getFavouriteFood() != null) {
                addAttribute(profile.getFavouriteFood());
            }
            if (profile.getFavouriteMusic() != null) {
                addAttribute(profile.getFavouriteMusic());
            }
            oldProfile = profile.copyProfile();
            adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
            attributeHolder.setAdapter(adapter);
            attributeHolder.setOnItemLongClickListener(adapter);
        } else {
            oldProfile = new Profile();
        }
    }

    private void addAttribute(final Attribute attribute) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = inflater.inflate(R.layout.simple_attribute, null);
        ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
        attrIcon.setImageDrawable(attribute.getAttributeIconResource(this));
        TextView description = (TextView) v.findViewById(R.id.description);
        description.setText(attribute.getTextualRepresentation());
        //ImageView delete = (ImageView) v.findViewById(R.id.delete);
        attributes.add(attribute);
        /*delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAttribute(attribute);
            }
        });*/
        adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
        attributeHolder.setAdapter(adapter);
        attributeHolder.setOnItemLongClickListener(adapter);
    }

    void deleteAttribute(Attribute attribute) {
        try {
            if (attribute instanceof Birthday) {
                if (((Birthday) attribute).getId() != null)
                    profile.getBirthday().delete();
                profile.setBirthday(null);
            }
            if (attribute instanceof WeddingDay) {
                if (((WeddingDay) attribute).getId() != null)
                    profile.getWeddingDay().delete();
                profile.setWeddingDay(null);
            }
            if (attribute instanceof Height) {
                if (((Height) attribute).getId() != null)
                    profile.getHeight().delete();
                profile.setHeight(null);
            }
            if (attribute instanceof Weight) {
                if (((Weight) attribute).getId() != null)
                    profile.getWeight().delete();
                profile.setWeight(null);
            }
            if (attribute instanceof FavouriteFlowers) {
                if (((FavouriteFlowers) attribute).getId() != null)
                    profile.getFavouriteFlowers().delete();
                profile.setFavouriteFlowers(null);
            }
            if (attribute instanceof ShoeSize) {
                if (((ShoeSize) attribute).getId() != null)
                    profile.getShoeSize().delete();
                profile.setShoeSize(null);
            }
            if (attribute instanceof ShirtSize) {
                if (((ShirtSize) attribute).getId() != null)
                    profile.getShirtSize().delete();
                profile.setShirtSize(null);
            }
            if (attribute instanceof FavouriteColor) {
                if (((FavouriteColor) attribute).getId() != null)
                    profile.getFavouriteColor().delete();
                profile.setFavouriteColor(null);
            }
            if (attribute instanceof MenstrualCalendar) {
                if (((MenstrualCalendar) attribute).getId() != null)
                    profile.getMenstrualCalendar().delete();
                profile.setMenstrualCalendar(null);
            }
            if (attribute instanceof TrousersSize) {
                if (((TrousersSize) attribute).getId() != null)
                    profile.getTrousersSize().delete();
                profile.setTrousersSize(null);
            }
            if (attribute instanceof TShirtSize) {
                if (((TShirtSize) attribute).getId() != null)
                    profile.gettShirtSize().delete();
                profile.settShirtSize(null);
            }
            if (attribute instanceof FavouriteBook) {
                if (((FavouriteBook) attribute).getId() != null)
                    profile.getFavouriteBook().delete();
                profile.setFavouriteBook(null);
            }
            if (attribute instanceof FavouriteDrink) {
                if (((FavouriteDrink) attribute).getId() != null)
                    profile.getFavouriteBook().delete();
                profile.setFavouriteDrink(null);
            }
            if (attribute instanceof FavouriteFood) {
                if (((FavouriteFood) attribute).getId() != null)
                    profile.getFavouriteFood().delete();
                profile.setFavouriteFood(null);
            }
            if (attribute instanceof FavouriteMusic) {
                if (((FavouriteMusic) attribute).getId() != null)
                    profile.getFavouriteMusic().delete();
                profile.setFavouriteMusic(null);
            }
        } catch (NullPointerException e) {
        }
        attributes.remove(attribute);
        adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
        attributeHolder.setAdapter(adapter);
        attributeHolder.setOnItemLongClickListener(adapter);
    }

    private int findRelationSelectionByName(String relation) {
        String[] relations = filterOutByGender(Constants.relations, male.isChecked());
        int position = 0;
        for (String color : relations) {
            if (color.equals(relation)) return position;
            else position++;
        }
        return relations.length - 1;
    }

    private int findEyeColorSelectionByName(String eyeColor) {
        String[] eyeColors = getResources().getStringArray(R.array.eye_colors);
        int position = 0;
        for (String color : eyeColors) {
            if (color.equals(eyeColor)) return position;
            else position++;
        }
        return eyeColors.length - 1;
    }

    private int findHairColorSelectionByName(String hairColor) {
        String[] hairColors = getResources().getStringArray(R.array.hair_colors);
        int position = 0;
        for (String color : hairColors) {
            if (color.equals(hairColor)) return position;
            else position++;
        }
        return hairColors.length - 1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            return checkIfDirty();
        } else if (id == R.id.save) {
            new ProfileSaver(false).execute();
            return true;
        } else if (id == R.id.delete) {
            new MaterialDialog.Builder(this)
                    .title(R.string.delete_attributes)
                    .content(R.string.are_you_sure_delete)
                    .positiveText(android.R.string.yes)
                    .negativeText(android.R.string.no)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                            for (int position : adapter.getSelectedItems()) {
                                Attribute a = attributes.get(position);
                                deleteAttribute(a);
                            }
                        }
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onGenderRadioButtonClick(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.female:
                if (checked)
                    break;
            case R.id.male:
                if (checked)
                    break;
        }
    }

    public void onPINTypeChangedClick(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.global_pin:
                if (checked)
                    pinEditText.setVisibility(View.GONE);
                pinTypeChooser.setVisibility(View.GONE);
                break;
            case R.id.specific_pin:
                if (checked)
                    pinEditText.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Filters out options for relations, based on supplied gender
     *
     * @param relations     string array with all relations
     * @param isMaleChecked is this male
     * @return filtered array of relations
     */
    private String[] filterOutByGender(String[] relations, boolean isMaleChecked) {
        ArrayList<String> tempArray = new ArrayList<String>(Arrays.asList(relations));
        if (isMaleChecked) {
            tempArray.remove(getString(R.string.girlfriend));
            tempArray.remove(getString(R.string.sister));
            tempArray.remove(getString(R.string.mother));
            tempArray.remove(getString(R.string.grandmother));
            tempArray.remove(getString(R.string.spouse));
        } else {
            tempArray.remove(getString(R.string.boyfriend));
            tempArray.remove(getString(R.string.brother));
            tempArray.remove(getString(R.string.father));
            tempArray.remove(getString(R.string.grandfather));
            tempArray.remove(getString(R.string.lover));
        }
        return tempArray.toArray(new String[tempArray.size()]);
    }


    private void handleAttributeClick(final int position) {
        String metric = getString(R.string.metric);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String units = prefs.getString("prefered_units", metric);
        switch (position) {
            case 0://birthday
            case 1://wedding day
                new DatePickerFragment(position).show(getSupportFragmentManager(), "datePicker");
                break;
            case 2://height
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_height)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(units.equals(metric) ? R.string.height_metric : R.string.height_imperial, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                Integer val = null;
                                try {
                                    val = Integer.valueOf(input.toString());
                                } catch (NumberFormatException e) {
                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_invalid_number), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (val <= 0) {
                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_height_cant_be_neg), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                final Height height = new Height();
                                height.setHeight(val);
                                profile.setHeight(height);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(height.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(height.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = height;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setHeight(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(height);
                                adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(adapter);
                                attributeHolder.setOnItemLongClickListener(adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 3: //weight
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_weight)
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input(units.equals(metric) ? R.string.weight_metric : R.string.weight_imperial, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                Integer val = null;
                                try {
                                    val = Integer.valueOf(input.toString());
                                } catch (NumberFormatException e) {
                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_invalid_number), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (val <= 0) {
                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_weight_cant_be_neg), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                final Weight weight = new Weight();
                                weight.setWeight(val);
                                profile.setWeight(weight);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(weight.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(weight.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = weight;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setWeight(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(weight);
                                adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(adapter);
                                attributeHolder.setOnItemLongClickListener(adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 4://favourite flowers
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_favourite_flower)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input(R.string.flower_name, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                final FavouriteFlowers favouriteFlowers = new FavouriteFlowers();
                                favouriteFlowers.setFlowerName(input.toString());
                                profile.setFavouriteFlowers(favouriteFlowers);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(favouriteFlowers.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(favouriteFlowers.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = favouriteFlowers;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setFavouriteFlowers(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(favouriteFlowers);
                                adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(adapter);
                                attributeHolder.setOnItemLongClickListener(adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 5://shoe size
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .customView(R.layout.shoe_size_layout, true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                                Double val = null;
                                EditText size = (EditText) materialDialog.findViewById(R.id.size);
                                try {
                                    val = Double.valueOf(size.getText().toString());
                                } catch (NumberFormatException e) {
                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_invalid_decimal_number), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                final ShoeSize shoeSize = new ShoeSize();
                                shoeSize.setSize((int) Math.round(val));
                                Spinner type = (Spinner) materialDialog.findViewById(R.id.dropdown);
                                shoeSize.setOrigin(type.getSelectedItem().toString());
                                profile.setShoeSize(shoeSize);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(shoeSize.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(shoeSize.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = shoeSize;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setShoeSize(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(shoeSize);
                                adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(adapter);
                                attributeHolder.setOnItemLongClickListener(adapter);
                                dataDirty = true;
                                materialDialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 6://shirt size
                final ListAdapter adapter = new SizeChooserAdapter();
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .customView(R.layout.shirt_size_layout, true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                                EditText input = (EditText) materialDialog.findViewById(R.id.size);
                                Spinner type = (Spinner) materialDialog.findViewById(R.id.dropdown);
                                final ShirtSize shirtSize = new ShirtSize();
                                shirtSize.setSize(input.getText().toString());
                                shirtSize.setOrigin(type.getSelectedItem().toString());
                                profile.setShirtSize(shirtSize);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(shirtSize.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(shirtSize.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = shirtSize;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setShirtSize(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(shirtSize);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                materialDialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                                materialDialog.dismiss();
                            }
                        })
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .autoDismiss(false)
                        .cancelable(true)
                        .show();
                break;
            case 7://favourite color
                ColorPickerDialogBuilder
                        .with(this)
                        .setTitle("Choose color")
                        .initialColor(Color.WHITE)
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setPositiveButton(getString(android.R.string.ok), new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                                FavouriteColor color = new FavouriteColor();
                                color.setColor(selectedColor);
                                profile.setFavouriteColor(color);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(color.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(color.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = color;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setFavouriteColor(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(color);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .build()
                        .show();
                break;
            case 8:
                final MaterialDialog dialog = new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .customView(R.layout.menstrual_input, false)
                        .autoDismiss(true)
                        .cancelable(true)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .build();
                final EditText days = (EditText) dialog.findViewById(R.id.cycle_length);
                final Button selectDate = (Button) dialog.findViewById(R.id.start_date);
                selectDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DatePickerFragment(position).show(getSupportFragmentManager(), "datePicker");
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    synchronized (menstLock) {
                                        menstLock.wait();
                                    }
                                    MenstrualCalendar mc = profile.getMenstrualCalendar();
                                    if (mc == null) {
                                        Toast.makeText(AddProfileActivity.this, getString(R.string.error_last_start_date_req), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    if (days.getText().toString().equals("")) {
                                        profile.setMenstrualCalendar(null);
                                        Toast.makeText(AddProfileActivity.this, getString(R.string.error_days_are_required), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    mc.setAvgLengthOfPeriod(Integer.parseInt(days.getText().toString()));
                                    dataDirty = true;
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }
                        }.start();

                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case 9://trousers
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_trousers_size)
                        .inputType(InputType.TYPE_NUMBER_FLAG_DECIMAL)
                        .input(R.string.trousers_size, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                Double val = null;
                                try {
                                    val = Double.valueOf(input.toString());
                                } catch (NumberFormatException e) {
                                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_invalid_decimal_number), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                final TrousersSize trousersSize = new TrousersSize();
                                trousersSize.setSize((int) Math.round(val));
                                trousersSize.setSubSize((int) (val - Math.floor(val)) * 100);
                                profile.setTrousersSize(trousersSize);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(trousersSize.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(trousersSize.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = trousersSize;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setTrousersSize(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(trousersSize);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 10://tshirt
                final ListAdapter tadapter = new SizeChooserAdapter();
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.choose_tshirt_size)
                        .adapter(tadapter, new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                String chosenItem = (String) tadapter.getItem(position);
                                final TShirtSize tshirtSize = new TShirtSize();
                                tshirtSize.setSize(chosenItem);
                                profile.settShirtSize(tshirtSize);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(tshirtSize.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(tshirtSize.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = tshirtSize;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.settShirtSize(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(tshirtSize);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .show();
                break;
            case 11://book
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_favourite_book)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input(R.string.name, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                final FavouriteBook favouriteBook = new FavouriteBook();
                                favouriteBook.setBookName(input.toString());
                                profile.setFavouriteBook(favouriteBook);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(favouriteBook.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(favouriteBook.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = favouriteBook;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setFavouriteBook(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(favouriteBook);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 12://drink
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_favourite_drink)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input(R.string.name, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                final FavouriteDrink favouriteDrink = new FavouriteDrink();
                                favouriteDrink.setDrink(input.toString());
                                profile.setFavouriteDrink(favouriteDrink);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(favouriteDrink.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(favouriteDrink.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = favouriteDrink;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setFavouriteDrink(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(favouriteDrink);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 13://food
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_favourite_food)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input(R.string.name, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                final FavouriteFood favouriteFood = new FavouriteFood();
                                favouriteFood.setFoodName(input.toString());
                                profile.setFavouriteFood(favouriteFood);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(favouriteFood.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(favouriteFood.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = favouriteFood;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setFavouriteFood(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(favouriteFood);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;
            case 14://music
                new MaterialDialog.Builder(AddProfileActivity.this)
                        .title(getString(R.string.attribute))
                        .content(R.string.input_favourite_music)
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input(R.string.name, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                final FavouriteMusic favouriteMusic = new FavouriteMusic();
                                favouriteMusic.setName(input.toString());
                                profile.setFavouriteMusic(favouriteMusic);
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View v = inflater.inflate(R.layout.simple_attribute, null);
                                ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
                                attrIcon.setImageDrawable(favouriteMusic.getAttributeIconResource(AddProfileActivity.this));
                                TextView description = (TextView) v.findViewById(R.id.description);
                                description.setText(favouriteMusic.getTextualRepresentation());
                                //ImageView delete = (ImageView) v.findViewById(R.id.delete);
                                final Attribute toBeDeleted = favouriteMusic;
                                /*delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        profile.setFavouriteMusic(null);
                                        attributeHolder.removeView(v);
                                    }
                                });*/
                                attributes.add(favouriteMusic);
                                AddProfileActivity.this.adapter = new rs.hump.aboutmy.profile.AttributeAdapter(AddProfileActivity.this, attributes, profile, attributeHolder);
                                attributeHolder.setAdapter(AddProfileActivity.this.adapter);
                                attributeHolder.setOnItemLongClickListener(AddProfileActivity.this.adapter);
                                dataDirty = true;
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
                break;

        }
    }

    public Profile getProfile() {
        return profile;
    }

    private class AttributeAdapter implements ListAdapter {

        private List<Attribute> attributes;

        public AttributeAdapter(List<Attribute> attributes) {
            this.attributes = attributes;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return attributes.size();
        }

        @Override
        public Object getItem(int position) {
            return attributes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View recycledView = convertView;
            final Attribute item = (Attribute) getItem(position);
            if (recycledView == null) {
                recycledView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_attribute, null);
            }
            ImageView icon = (ImageView) recycledView.findViewById(R.id.icon);
            if (item instanceof ShoeSize && female.isChecked()) {
                icon.setImageResource(R.drawable.ic_heel);
            } else {
                icon.setImageDrawable(item.getAttributeIconResource(AddProfileActivity.this));
            }
            TextView description = (TextView) recycledView.findViewById(R.id.description);
            description.setText(item.getClass().getSimpleName());
            return recycledView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

    }

    public class DatePickerFragment extends AppCompatDialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private int attrIndex;

        public DatePickerFragment(int attrIndex) {
            this.attrIndex = attrIndex;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            long today = c.getTimeInMillis();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, year, month, day);
            dpd.getDatePicker().setMaxDate(today);
            return dpd;
        }

        public void onDateSet(final DatePicker view, int year, int month, int day) {
            Attribute attribute = null;
            switch (attrIndex) {
                case 0:
                    Birthday bday = new Birthday();
                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, month);
                    cal.set(Calendar.DAY_OF_MONTH, day);
                    bday.setBirthday(cal.getTimeInMillis());
                    attribute = bday;
                    profile.setBirthday(bday);
                    break;
                case 1:
                    WeddingDay wday = new WeddingDay();
                    cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, month);
                    cal.set(Calendar.DAY_OF_MONTH, day);
                    wday.setWeddingDay(cal.getTimeInMillis());
                    attribute = wday;
                    profile.setWeddingDay(wday);
                    break;
                case 8:
                    MenstrualCalendar mc = new MenstrualCalendar();
                    cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, month);
                    cal.set(Calendar.DAY_OF_MONTH, day);
                    mc.setStart(cal.getTimeInMillis());
                    attribute = mc;
                    profile.setMenstrualCalendar(mc);
                    new Thread() {
                        @Override
                        public void run() {
                            synchronized (menstLock) {
                                menstLock.notify();
                            }
                        }
                    }.start();
                    break;
            }

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View v = inflater.inflate(R.layout.simple_attribute, null);
            ImageView attrIcon = (ImageView) v.findViewById(R.id.icon);
            attrIcon.setImageDrawable(attribute.getAttributeIconResource(AddProfileActivity.this));
            TextView description = (TextView) v.findViewById(R.id.description);
            description.setText(attribute.getTextualRepresentation());
            //ImageView delete = (ImageView) v.findViewById(R.id.delete);
            final Attribute toBeDeleted = attribute;
            /*delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteAttribute(toBeDeleted);
                }
            });*/
            addAttribute(attribute);

        }
    }

    private class SizeChooserAdapter implements ListAdapter {

        private String[] ss = ShirtSize.shirtSizes;

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return ss.length;
        }

        @Override
        public Object getItem(int position) {
            return ss[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View recycledView = convertView;
            if (recycledView == null) {
                recycledView = getLayoutInflater().inflate(android.R.layout.simple_dropdown_item_1line, null);
            }
            TextView tv = ((TextView) recycledView);
            tv.setGravity(Gravity.CENTER);
            tv.setText((String) getItem(position));
            return recycledView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return attributes.isEmpty();
        }
    }

    private ProgressDialog myProgressDialog;

    private class ProfileSaver extends AsyncTask<Integer, Integer, Boolean> {

        private boolean finishOnExit;

        public ProfileSaver(boolean finishOnExit) {
            this.finishOnExit = finishOnExit;
        }

        protected void onPreExecute() {
            myProgressDialog = new ProgressDialog(AddProfileActivity.this);
            myProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            myProgressDialog.setMessage(AddProfileActivity.this.getString(R.string.saving_profile));
            myProgressDialog.setCancelable(false);
            myProgressDialog.setIndeterminate(false);
            myProgressDialog.setMax(100);
            myProgressDialog.setProgress(0);
            myProgressDialog.show();
        }

        protected Boolean doInBackground(Integer... params) {
            try {
                int currentProgress = 0;
                if (profileName.getText().toString().isEmpty()) {//can't be empty
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AddProfileActivity.this, getString(R.string.error_profile_name_empty), Toast.LENGTH_SHORT).show();
                        }
                    });
                    profileName.requestFocus();
                    return null;
                }
                if (!male.isChecked() && !female.isChecked()) {//must be checked
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AddProfileActivity.this, getString(R.string.error_must_check_gender), Toast.LENGTH_SHORT).show();
                        }
                    });
                    return null;
                }
                profile.setName(profileName.getText().toString());
                profile.setGender(male.isChecked() ? "male" : "female");
                profile.setRelation((String) relation.getSelectedItem());
                profile.setEyeColor((String) eyeColor.getSelectedItem());
                profile.setHairColor((String) hairColor.getSelectedItem());
                profile.setPrivateProfile(privateProfile.isChecked() ? 1 : 0);
                if (privateProfile.isChecked()) {
                    if (globalPin.isChecked()) {
                        profile.setGlobalPIN(1);
                        profile.setPrivateProfile(1);
                        profile.setPIN("");
                    } else {
                        profile.setGlobalPIN(0);
                        profile.setPrivateProfile(1);
                        profile.setPIN(pinEditText.getText().toString());
                    }
                } else {
                    profile.setGlobalPIN(0);
                    profile.setPrivateProfile(0);
                    profile.setPIN("");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.getInstance().addGlobalProfile(profile);
                        }
                    });
                }
                if (profile.getPrivateProfile() == 1)
                    profile.save();//partial save

                currentProgress += 25;
                publishProgress(currentProgress);
                if (oldProfile.getBirthday() == null && profile.getBirthday() != null) {
                    profile.getBirthday().save();
                    addTimeNotification(profile.getBirthday().getBirthday(), String.format("%s %s", getString(R.string.birhtday_notification), profile.getName()), profile.getBirthday().getId());
                }
                if (oldProfile.getWeddingDay() == null && profile.getWeddingDay() != null) {
                    profile.getWeddingDay().save();
                    addTimeNotification(profile.getWeddingDay().getWeddingDay(), String.format("%s", getString(R.string.wedding_notification)), profile.getWeddingDay().getId());
                }
                if (profile.getHeight() != null) profile.getHeight().save();
                if (profile.getWeight() != null) profile.getWeight().save();
                if (profile.getFavouriteFlowers() != null) profile.getFavouriteFlowers().save();
                if (profile.getShoeSize() != null) profile.getShoeSize().save();
                currentProgress += 25;
                publishProgress(currentProgress);
                if (profile.getShirtSize() != null) profile.getShirtSize().save();
                if (profile.getFavouriteColor() != null) profile.getFavouriteColor().save();
                if (profile.getMenstrualCalendar() != null) profile.getMenstrualCalendar().save();
                if (profile.getTrousersSize() != null) profile.getTrousersSize().save();
                if (profile.gettShirtSize() != null) profile.gettShirtSize().save();
                currentProgress += 25;
                publishProgress(currentProgress);
                if (profile.getFavouriteBook() != null) profile.getFavouriteBook().save();
                if (profile.getFavouriteDrink() != null) profile.getFavouriteDrink().save();
                if (profile.getFavouriteFood() != null) profile.getFavouriteFood().save();
                if (profile.getFavouriteMusic() != null) profile.getFavouriteMusic().save();

                if (profile.getPrivateProfile() == 1)
                    profile.save();
                else {
                    ArrayList<Profile> publicProfiles = RefreshableActivity.getPublicProfiles(AddProfileActivity.this);
                    long newId = -1;
                    for (Profile p : publicProfiles) {
                        if (p.getId() != null && p.getId() < newId) newId = p.getId();
                    }
                    newId -= 1;
                    profile.setId(newId);
                }
                dataDirty = false;
                AbstractNotification notification = new AbstractNotification();
                Calendar cal = Calendar.getInstance();

                //monthly remainder
                if (!doNotShowRemainder.isChecked() && oldProfile.getId() == null) {
                    cal.add(Calendar.DATE, 25 + ((int) Math.random() * 5));
                    long launchTime = getNotificationTime(cal);
                    notification.setDue(launchTime);
                    notification.setPrivateProfile(profile.getPrivateProfile());
                    Notification monthlyRemainder = new Notification();
                    monthlyRemainder.setDueTime(launchTime);
                    monthlyRemainder.setProfile(profile);
                    monthlyRemainder.setLength(Constants.DAY);
                    int style = randomGenerator.nextInt(3);
                    switch (style) {
                        case 0:
                            monthlyRemainder.setMessage("Have you called " + profile.getName() + " recently?");
                            break;
                        case 1:
                            monthlyRemainder.setMessage("Have you heard from " + profile.getName() + " recently?");
                            break;
                        case 2:
                            monthlyRemainder.setMessage("Is it time to talk to " + profile.getName() + "?");
                            break;
                    }
                    monthlyRemainder.setAttributeId(0);//always show
                    TipMessage.findTipMessageAndPutInNotification(profile, monthlyRemainder);
                    if (monthlyRemainder.getProfile().getPrivateProfile() == 1) {
                        monthlyRemainder.save();
                    } else {
                        List<Notification> notifications1 = RefreshableActivity.readAllNotes(AddProfileActivity.this);
                        notifications1.add(monthlyRemainder);
                        long newId = -1;
                        for (Notification n1 : notifications1) {
                            if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
                        }
                        newId--;
                        monthlyRemainder.setId(newId);
                        RefreshableActivity.writeAllNotes(AddProfileActivity.this, notifications1);
                    }
                    notification.setNotificationId(monthlyRemainder.getId());
                    MainActivity.addNotificationToQueue(notification, AddProfileActivity.this);
                }

                //add notifications for menstrual calendar
                if (oldProfile.getMenstrualCalendar() == null && profile.getMenstrualCalendar() != null) {
                    //menstrual cycle will begin
                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR, 23);
                    now.set(Calendar.MINUTE, 59);//set for end of day
                    Calendar menstStart = Calendar.getInstance();
                    MenstrualCalendar mc = profile.getMenstrualCalendar();
                    menstStart.setTimeInMillis(mc.getStart());
                    if (menstStart.compareTo(now) <= 0) {
                        menstStart.add(Calendar.DATE, mc.getAvgLengthOfPeriod());//show next
                    }
                    //three days before
                    menstStart.add(Calendar.DATE, -3);

                    //setup notification
                    Notification n = new Notification();
                    n.setMessage(getString(R.string.menstrual_cycle_is_begining));
                    n.setDueTime(menstStart.getTimeInMillis());
                    n.setPeriod(mc.getAvgLengthOfPeriod() * Constants.DAY);
                    n.setLength(Constants.DAY * 3);
                    n.setProfile(profile);
                    n.setAttributeId(mc.getId());
                    TipMessage.findTipMessageAndPutInNotification(profile, n);
                    if (n.getProfile().getPrivateProfile() == 1) {
                        n.save();
                    } else {
                        List<Notification> notifications1 = RefreshableActivity.readAllNotes(AddProfileActivity.this);
                        notifications1.add(n);
                        long newId = -1;
                        for (Notification n1 : notifications1) {
                            if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
                        }
                        newId--;
                        n.setId(newId);
                        RefreshableActivity.writeAllNotes(AddProfileActivity.this, notifications1);
                    }
                    profile.getNotifications().add(n);
                    AbstractNotification an = new AbstractNotification();
                    an.setDue(menstStart.getTimeInMillis());
                    an.setNotificationId(n.getId());
                    an.setPrivateProfile(profile.getPrivateProfile());
                    MainActivity.addNotificationToQueue(an, AddProfileActivity.this);
                    //three days before

                    //menstrual cycle began
                    menstStart.add(Calendar.DATE, 3);

                    //setup notification
                    n = new Notification();
                    n.setMessage(getString(R.string.menstrual_cycle_began));
                    n.setDueTime(menstStart.getTimeInMillis());
                    n.setPeriod(mc.getAvgLengthOfPeriod() * Constants.DAY);
                    n.setLength(Constants.DAY);
                    n.setProfile(profile);
                    n.setAttributeId(mc.getId());
                    TipMessage.findTipMessageAndPutInNotification(profile, n);
                    if (n.getProfile().getPrivateProfile() == 1) {
                        n.save();
                    } else {
                        List<Notification> notifications1 = RefreshableActivity.readAllNotes(AddProfileActivity.this);
                        notifications1.add(n);
                        long newId = -1;
                        for (Notification n1 : notifications1) {
                            if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
                        }
                        newId--;
                        n.setId(newId);
                        RefreshableActivity.writeAllNotes(AddProfileActivity.this, notifications1);
                    }
                    profile.getNotifications().add(n);
                    an = new AbstractNotification();
                    an.setDue(menstStart.getTimeInMillis());
                    an.setNotificationId(n.getId());
                    an.setPrivateProfile(profile.getPrivateProfile());
                    MainActivity.addNotificationToQueue(an, AddProfileActivity.this);

                    //fertile days

                    menstStart.setTime(mc.getOvulationDate());
                    menstStart.add(Calendar.DATE, -6);

                    //setup notification
                    n = new Notification();
                    n.setMessage(getString(R.string.fertile_days_begining_soon));
                    n.setDueTime(menstStart.getTimeInMillis());
                    n.setPeriod(mc.getAvgLengthOfPeriod() * Constants.DAY);
                    n.setLength(Constants.DAY * 3);
                    n.setProfile(profile);
                    n.setAttributeId(mc.getId());
                    TipMessage.findTipMessageAndPutInNotification(profile, n);
                    if (n.getProfile().getPrivateProfile() == 1) {
                        n.save();
                    } else {
                        List<Notification> notifications1 = RefreshableActivity.readAllNotes(AddProfileActivity.this);
                        notifications1.add(n);
                        long newId = -1;
                        for (Notification n1 : notifications1) {
                            if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
                        }
                        newId--;
                        n.setId(newId);
                        RefreshableActivity.writeAllNotes(AddProfileActivity.this, notifications1);
                    }
                    profile.getNotifications().add(n);
                    an = new AbstractNotification();
                    an.setDue(menstStart.getTimeInMillis());
                    an.setNotificationId(n.getId());
                    an.setPrivateProfile(profile.getPrivateProfile());
                    MainActivity.addNotificationToQueue(an, AddProfileActivity.this);

                    menstStart.add(Calendar.DATE, 3);
                    //setup notification
                    n = new Notification();
                    n.setMessage(getString(R.string.fertile_days_began));
                    n.setDueTime(menstStart.getTimeInMillis());
                    n.setPeriod(mc.getAvgLengthOfPeriod() * Constants.DAY);
                    n.setLength(Constants.DAY);
                    n.setProfile(profile);
                    n.setAttributeId(mc.getId());
                    TipMessage.findTipMessageAndPutInNotification(profile, n);
                    if (n.getProfile().getPrivateProfile() == 1) {
                        n.save();
                    } else {
                        List<Notification> notifications1 = RefreshableActivity.readAllNotes(AddProfileActivity.this);
                        notifications1.add(n);
                        long newId = -1;
                        for (Notification n1 : notifications1) {
                            if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
                        }
                        newId--;
                        n.setId(newId);
                        RefreshableActivity.writeAllNotes(AddProfileActivity.this, notifications1);
                    }
                    profile.getNotifications().add(n);
                    an = new AbstractNotification();
                    an.setDue(menstStart.getTimeInMillis());
                    an.setNotificationId(n.getId());
                    an.setPrivateProfile(profile.getPrivateProfile());
                    MainActivity.addNotificationToQueue(an, AddProfileActivity.this);
                }

                MainActivity.getInstance().addMasterProfile(profile);
                currentProgress += 25;
                publishProgress(currentProgress);
                Log.i(AddProfileActivity.class.getName(), "Profile saved: " + profile);

                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        protected void onProgressUpdate(Integer... progressPosition) {
            myProgressDialog.setProgress(progressPosition[0]);
        }

        protected void onPostExecute(Boolean result) {
            myProgressDialog.dismiss();
            if (result != null) {
                if (result) {
                    Toast.makeText(AddProfileActivity.this, getString(R.string.success_saving_profile), Toast.LENGTH_SHORT).show();
                    if (finishOnExit) AddProfileActivity.this.finish();
                } else {
                    Toast.makeText(AddProfileActivity.this, getString(R.string.error_saving_profile), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void addTimeNotification(long date, String message, long attributeId) {
        AbstractNotification abstractNotification = new AbstractNotification();
        Calendar now = Calendar.getInstance();
        Calendar bDay = Calendar.getInstance();
        bDay.setTimeInMillis(date);

        //add years up to now
        while (bDay.compareTo(now) <= 0) {
            bDay.add(Calendar.YEAR, 1);
        }

        Calendar limit = Calendar.getInstance();
        long launchTime = getNotificationTime(bDay);
        abstractNotification.setDue(launchTime - Constants.DAY); // day earlier
        abstractNotification.setPrivateProfile(profile.getPrivateProfile());

        //create notification
        Notification notification = new Notification();
        notification.setDueTime(launchTime - Constants.DAY);
        notification.setMessage(message);
        notification.setProfile(profile);
        notification.setLength(2 * Constants.DAY);
        notification.setAttributeId(attributeId);
        TipMessage.findTipMessageAndPutInNotification(profile, notification);
        if (profile.getPrivateProfile() == 1)
            notification.save();
        else {
            List<Notification> notifications1 = RefreshableActivity.readAllNotes(this);
            notifications1.add(notification);
            long newId = -1;
            for (Notification n1 : notifications1) {
                if (n1.getId() != null && n1.getId() < newId) newId = n1.getId();
            }
            newId--;
            notification.setId(newId);
            RefreshableActivity.writeAllNotes(this, notifications1);
        }
        profile.getNotifications().add(notification);
        abstractNotification.setNotificationId(notification.getId());
        //push notification in queue
        MainActivity.addNotificationToQueue(abstractNotification, AddProfileActivity.this);
    }

    public long getNotificationTime(Calendar cal) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String start = prefs.getString("notification_start_time", null);
        String end = prefs.getString("notification_end_time", null);
        if (start == null) start = "9:00";
        if (end == null) end = "21:00";
        cal.set(Calendar.HOUR, (AppCompatPreferenceActivity.getHour(start) - AppCompatPreferenceActivity.getHour(end)) / 2);
        cal.set(Calendar.MINUTE, (AppCompatPreferenceActivity.getMinute(start) - AppCompatPreferenceActivity.getMinute(end)) / 2);
        return cal.getTimeInMillis();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            checkIfDirty();
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean checkIfDirty() {
        if (dataDirty) {
            new MaterialDialog.Builder(AddProfileActivity.this)
                    .title(R.string.warning)
                    .content(R.string.confirm_exit_without_saving)
                    .positiveText(android.R.string.ok)
                    .negativeText(android.R.string.cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                            new ProfileSaver(true).execute();
                        }
                    })
                    .cancelable(false)
                    .show();
            return true;
        }
        return false;
    }

    @Override
    public void notifySelected(int size) {
        try {
            if (size == 0) {
                //remove delete icon
                toolbar.getMenu().getItem(1).setVisible(false);
            } else {
                //add delete icon
                toolbar.getMenu().getItem(1).setVisible(true);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
