package rs.hump.aboutmy.profile;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RefreshableActivity;
import rs.hump.aboutmy.RoboAppCompatActivity;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.domain.attributes.Attribute;
import rs.hump.aboutmy.domain.attributes.MenstrualCalendar;
import rs.hump.aboutmy.domain.attributes.ShoeSize;

/**
 * Adapter for grid view on profile dashboard.
 *
 * @author Dejan Markovic
 */
public class AttributeAdapter extends BaseAdapter implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private RoboAppCompatActivity activity;
    private List<Attribute> attributes;
    private Profile profile;
    private boolean deleteEnabled = true;
    private ArrayList<Integer> selected = new ArrayList<>();

    public AttributeAdapter(RoboAppCompatActivity activity, List<Attribute> attributes, Profile profile, AbsListView list) {
        this.activity = activity;
        this.attributes = attributes;
        this.profile = profile;
        Collections.sort(attributes, new Comparator<Attribute>() {
            @Override
            public int compare(Attribute lhs, Attribute rhs) {
                return lhs.getPriority() > rhs.getPriority() ? -1 : (lhs.getPriority() == rhs.getPriority() ? 0 : 1);
            }
        });

        if (list instanceof ListView)
            RefreshableActivity.setListViewHeightBasedOnChildren((ListView) list);
        activity.notifySelected(0);
    }

    @Override
    public int getCount() {
        return attributes.size();
    }

    @Override
    public Object getItem(int position) {
        return attributes.get(position);
    }

    @Override
    public long getItemId(int position) {
        //not used
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View recycledView = convertView;

        if (recycledView == null) {
            recycledView = activity.getLayoutInflater().inflate(R.layout.simple_attribute, null);
        }

        final Attribute item = (Attribute) getItem(position);

        //set attribute icon
        ImageView icon = (ImageView) recycledView.findViewById(R.id.icon);
        if (item instanceof MenstrualCalendar) {
            MenstrualCalendar mc = (MenstrualCalendar) item;
            icon.setImageResource(mc.getCurrentDayResource());
        }
        if (item instanceof ShoeSize && profile.getGender() != null && profile.getGender().equals(activity.getString(R.string.female))) {
            icon.setImageResource(R.drawable.ic_heel);
        } else {
            icon.setImageDrawable(item.getAttributeIconResource(activity));
        }
        //set attribute description
        TextView description = (TextView) recycledView.findViewById(R.id.description);
        description.setText(item.getTextualRepresentation());

        return recycledView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ((Attribute) getItem(position)).showDialogWithSummaryInfo(activity);
    }

    public void setDeleteEnabled(boolean deleteEnabled) {
        this.deleteEnabled = deleteEnabled;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (!this.selected.contains(new Integer(position))) {
            this.selected.add(new Integer(position));
            view.setBackgroundColor(ContextCompat.getColor(activity, R.color.Silver));
        } else {
            this.selected.remove(new Integer(position));
            view.setBackgroundColor(Color.TRANSPARENT);
        }
        activity.notifySelected(this.selected.size());
        return true;
    }

    public ArrayList<Integer> getSelectedItems() {
        return selected;
    }
}
