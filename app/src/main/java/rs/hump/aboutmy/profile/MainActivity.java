package rs.hump.aboutmy.profile;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.Database;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import roboguice.inject.InjectView;
import rs.hump.aboutmy.AboutMyApplication;
import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RefreshableActivity;
import rs.hump.aboutmy.domain.AbstractNotification;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.notifications.NotificationsListActivity;
import rs.hump.aboutmy.settings.SettingsActivity;

@SuppressWarnings("ALL")
public class MainActivity extends RefreshableActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //constants
    public static final String NOTIFICATION = "rs.hump.aboutmy.NOTIFICATION";
    private static final String NOTIFICATION_INSTANCE = "rs.hump.aboutmy.NOTIFICATION_INSTANCE";
    private static final int REQUEST_CODE = 1;
    private static final String NOTIFICATIONS_FILENAME = "notif.bin";

    @InjectView(R.id.profiles)
    private ListView profiles;
    int selectedPosition;
    private Profile selectedProfile;
    private static List<Profile> masterProfileList;
    private static List<Profile> profilesList = new ArrayList<>();
    private ProfileArrayAdapter adapter;
    private static String PIN;
    private static MainActivity instance;
    private static MaterialDialog dialog;
    private static boolean alreadyRedirected;

    public static MainActivity getInstance() {
        return instance;
    }

    public List<Profile> getMasterProfileList() {
        return masterProfileList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        ArrayList<Profile> list = getPublicProfiles(this);
        if (list != null)
            profilesList.addAll(list);
        Constants.dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddProfileActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        refresh(PIN);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("PIN", PIN);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        PIN = savedInstanceState.getString("PIN");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.unlock) {
            unlockProfile();
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        if (PIN != null)
            profilesList.addAll(Profile.listAll(Profile.class));
        masterProfileList = new ArrayList<>(profilesList);

        Iterator<Profile> profileListIterator = profilesList.iterator();
        while (profileListIterator.hasNext()) {
            Profile item = profileListIterator.next();
            if (!item.getPIN().equals("")) profileListIterator.remove();
        }

        profiles.setAdapter(adapter = new ProfileArrayAdapter());
        profiles.setOnItemClickListener(adapter);

        if (masterProfileList.isEmpty() && !alreadyRedirected) {//go straight to creating first profile
            alreadyRedirected = true;
            Intent intent = new Intent(this, AddProfileActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            PIN = null;
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add_new_profile) {
            Intent intent = new Intent(this, AddProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.unlock_private_profile) {
            unlockProfile();
        } else if (id == R.id.active_notifications) {
            Intent intent = new Intent(this, NotificationsListActivity.class);
            startActivity(intent);
        } else if (id == R.id.settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.about) {
            new MaterialDialog.Builder(MainActivity.this)
                    .title(getString(R.string.about))
                    .content("This application was made by inspiration of some inovative people in tavern!\nHappy using! :D")
                    .autoDismiss(true)
                    .cancelable(true)
                    .positiveText(android.R.string.ok)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void unlockProfile() {
        new MaterialDialog.Builder(MainActivity.this)
                .title(getString(R.string.profile_unlock))
                .content(R.string.input_content)
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                .input(R.string.pin, R.string.input_prefill, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        if (input.toString().equals("")) dialog.dismiss();
                        AboutMyApplication app = ((AboutMyApplication) (AboutMyApplication.getSugarContext()));
                        app.setEncryptionKey(input.toString());
                        try {
                            Database database = app.tryGetDatabase();
                            database.getDB();
                            PIN = input.toString();
                            refresh(input.toString());
                            dialog.dismiss();
                            return;
                        } catch (Exception e) {
                            //wrong password
                            dialog.show();
                            e.printStackTrace();
                        }
                        Profile unlocked = null;
                        //is it global PIN ?
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

                        for (Profile p : masterProfileList) {
                            if (p.getPrivateProfile() == 1 && p.getPIN().equals(input.toString())) {
                                unlocked = p;
                                break;
                            }
                        }
                        if (unlocked != null && !profilesList.contains(unlocked)) {
                            profilesList.add(unlocked);
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.error_wrong_pin), Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .autoDismiss(false)
                .cancelable(true)
                .negativeText(android.R.string.cancel)
                .show();
    }

    public void addGlobalProfile(Profile profile) {
        if (profilesList.contains(profile)) profilesList.remove(profile);
        profilesList.add(profile);
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        } else {
            profiles.setAdapter(adapter = new ProfileArrayAdapter());
            profiles.setOnItemClickListener(adapter);
        }
        savePublicProfiles(this, profilesList);
    }

    public void addMasterProfile(Profile profile) {
        if (masterProfileList.contains(profile)) masterProfileList.remove(profile);
        masterProfileList.add(profile);
    }

    public static void resetPIN() {
        PIN = null;
        if (instance != null) instance.finish();
    }

    @Override
    public void refresh(String PIN) {
        MainActivity.PIN = PIN;
        init();
    }

    public void removeProfile(Profile profile) {
        masterProfileList.remove(profile);
        profilesList.remove(profile);
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public List<Profile> getUnlockedProfiles() {
        return profilesList;
    }

    private class ProfileArrayAdapter extends ArrayAdapter<Profile> implements AdapterView.OnItemClickListener {
        public ProfileArrayAdapter() {
            super(MainActivity.this, R.layout.profile_item, profilesList);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View temp = convertView;
            if (temp == null) {
                temp = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.profile_item, null);
            }
            TextView profile_name = (TextView) temp.findViewById(R.id.profile_name);
            final Profile profile = getItem(position);
            profile_name.setText(profile.getName());
            profile_name.setTextColor(ContextCompat.getColor(MainActivity.this, profile.getGender().equals("male") ? R.color.RoyalBlue : R.color.Magenta));
            final ImageView icon = (ImageView) temp.findViewById(R.id.profile_icon);
            icon.setImageResource(getProfileImageResource(profile.getRelation()));

            /*icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedProfile = profile;
                    selectedPosition = position;
                    PhotoPickerIntent intent = new PhotoPickerIntent(MainActivity.this);
                    intent.setPhotoCount(1);
                    intent.setShowCamera(true);
                    intent.setShowGif(true);
                    startActivityForResult(intent, REQUEST_CODE);
                }
            });*/
            ImageView delete = (ImageView) temp.findViewById(R.id.profile_status);
            if (profile.getPrivateProfile() == 1) {
                delete.setImageResource(R.drawable.ic_unlocked);
                delete.setEnabled(true);
            } else {
                delete.setImageDrawable(null);
                delete.setEnabled(false);
            }
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeProfile(profile);
                }
            });

            return temp;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Profile profile = getItem(position);
            Intent intent = new Intent(getContext(), ProfileDashboard.class);
            intent.putExtra(AddProfileActivity.PROFILE, (Parcelable) profile);
            startActivity(intent);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            PIN = null;
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }

    static int getProfileImageResource(String relation) {
        switch (relation) {
            case "Boyfriend":
                return R.drawable.ic_boyfriend;
            case "Girlfriend":
                return R.drawable.ic_girlfriend;
            case "Lover":
                return R.drawable.ic_lover;
            case "Friend":
                return R.drawable.ic_friend;
            case "Spouse":
                return R.drawable.ic_spouse;
            case "Mother":
                return R.drawable.ic_mother;
            case "Father":
                return R.drawable.ic_father;
            case "Grandmother":
                return R.drawable.ic_grandmother;
            case "Grandfather":
                return R.drawable.ic_grandfather;
            case "Brother":
                return R.drawable.ic_brother;
            case "Sister":
                return R.drawable.ic_sister;
            default:
                return R.drawable.ic_profile;
        }
    }

    /**
     * Schedules bar notification for specified time.
     *
     * @param notification Notification to be scheduled
     * @param context      In which context are we scheduling notification
     */
    public static void addNotificationToQueue(AbstractNotification notification, Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(NOTIFICATION);
        //intent.putExtra(NOTIFICATION_INSTANCE, (Parcelable) notification);
        long due = notification.getDue();
        PendingIntent alarmSender = PendingIntent.getBroadcast(context, (int) due, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, due, alarmSender);

        //save to internal memory
        ArrayList<AbstractNotification> notifications = readFromInternalStorage(context);
        if (!notifications.contains(notification)) {
            notifications.add(notification);
            saveToInternalStorage(context, notifications);
        }
    }

    public static void saveToInternalStorage(Context ctx, ArrayList<AbstractNotification> notifications) {
        try {
            FileOutputStream fos = ctx.openFileOutput(NOTIFICATIONS_FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream of = new ObjectOutputStream(fos);
            of.writeObject(notifications);
            of.flush();
            of.close();
            fos.close();
        } catch (Exception e) {
            Log.e("WriteInternalStorage", "Error while saving");
        }
    }

    public static ArrayList<AbstractNotification> readFromInternalStorage(Context ctx) {
        try {
            FileInputStream fis = ctx.openFileInput(NOTIFICATIONS_FILENAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<AbstractNotification> list = (ArrayList<AbstractNotification>) ois.readObject();
            ois.close();
            return list;
        } catch (Exception e) {
            Log.e("ReadInternalStorage", "Error while reading");
        }
        return new ArrayList<>();//return empty
    }
}
