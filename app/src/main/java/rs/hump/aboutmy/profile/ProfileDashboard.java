package rs.hump.aboutmy.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.orm.Database;

import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import roboguice.inject.InjectView;
import rs.hump.aboutmy.AboutMyApplication;
import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.RefreshableActivity;
import rs.hump.aboutmy.RoboAppCompatActivity;
import rs.hump.aboutmy.domain.AbstractNotification;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.domain.attributes.Attribute;

public class ProfileDashboard extends RoboAppCompatActivity {

    private static final int REQUEST_CODE = 1;
    @InjectView(R.id.toolbar)
    private Toolbar toolbar;
    @InjectView(R.id.icon)
    private ImageView icon;
    @InjectView(R.id.notifications)
    private ListView notifications;
    private TextView footer;
    private Profile unlocked;
    private Profile profile;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_dashboard);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        profile = intent.getParcelableExtra(AddProfileActivity.PROFILE);
        if (profile == null && AddProfileActivity.getInstance() != null) {
            profile = AddProfileActivity.getInstance().getProfile();
        }
        final AbstractNotification notification = intent.getParcelableExtra("NOTIFICATION");
        if (notification != null) {
            //passed notification
            if (notification.getPrivateProfile() == 1) {
                //must authenticate
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.profile_unlock))
                        .content(R.string.input_content)
                        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        .input(R.string.pin, R.string.input_prefill, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {

                                if (input.toString().equals("")) dialog.dismiss();
                                AboutMyApplication app = ((AboutMyApplication) (AboutMyApplication.getSugarContext()));
                                app.setEncryptionKey(input.toString());
                                try {
                                    Database database = app.tryGetDatabase();
                                    database.getDB();
                                    MainActivity.getInstance().refresh(input.toString());
                                    List<Notification> notif = Notification.find(Notification.class, "id = ?", notification.getNotificationId() + "");
                                    final Notification n = notif.get(0);
                                    if (n.getProfile().getPIN() == null || !n.getProfile().getPIN().equals("")) {
                                        new MaterialDialog.Builder(ProfileDashboard.this)
                                                .title(getString(R.string.private_pin_unlock))
                                                .content(R.string.input_content)
                                                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                                                .input(R.string.pin, R.string.input_prefill, new MaterialDialog.InputCallback() {
                                                    @Override
                                                    public void onInput(MaterialDialog dialog, CharSequence input) {
                                                        if (n.getProfile().getPIN().equals(input.toString())) {
                                                            unlocked = n.getProfile();
                                                        } else {
                                                            Toast.makeText(ProfileDashboard.this, getString(R.string.error_wrong_pin), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                })
                                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        dialog.dismiss();
                                                        finish();
                                                    }
                                                })
                                                .autoDismiss(false)
                                                .cancelable(true)
                                                .negativeText(android.R.string.cancel)
                                                .show();
                                    }
                                    dialog.dismiss();
                                    return;
                                } catch (Exception e) {
                                    //wrong password
                                    dialog.show();
                                    e.printStackTrace();
                                }

                                List<Profile> profiles = MainActivity.getInstance().getUnlockedProfiles();
                                if (unlocked != null) {
                                    List<Profile> masterProfileList = MainActivity.getInstance().getMasterProfileList();
                                    for (Profile p : masterProfileList) {
                                        if (p.getPrivateProfile() == 1 && p.getPIN().equals(input.toString())) {
                                            unlocked = p;
                                            break;
                                        }
                                    }
                                }
                                if (unlocked != null && !profiles.contains(unlocked)) {
                                    profile = unlocked;
                                    dialog.dismiss();
                                } else {
                                    finish();
                                    Toast.makeText(ProfileDashboard.this, getString(R.string.error_wrong_pin), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .autoDismiss(false)
                        .cancelable(true)
                        .negativeText(android.R.string.cancel)
                        .show();
            } else {
                for (Profile p : RefreshableActivity.getPublicProfiles(this)) {
                    if (notification.getPrivateProfile() == p.getId()) {
                        profile = p;
                        break;
                    }
                }
            }
            List<Notification> list = Notification.find(Notification.class, "id = ?", notification.getNotificationId() + "");
            if (!list.isEmpty()) {
                Notification n = list.get(0);
            }
        }

        if (profile != null && (profile.getNotifications() == null || profile.getNotifications().isEmpty())) {
            List<Notification> notes = null;
            if(profile.getId() != null) {
                notes = Notification.findWithQuery(Notification.class, "Select * from Notification where profile = ?", String.valueOf(profile.getId()));
            }else{
                notes = RefreshableActivity.readAllNotes(this);
            }
            Iterator<Notification> notesNotificationIterator = notes.iterator();
            long now = Calendar.getInstance().getTimeInMillis();
            while (notesNotificationIterator.hasNext()) {
                boolean removed = false;
                Notification item = notesNotificationIterator.next();
                if (item.getDueTime() + item.getLength() < now) {
                    removed = true;
                    item.delete();//already passed
                    notesNotificationIterator.remove();
                }
                /*if (item.getDueTime() > now && item.getDueTime() + item.getLength() > now){
                    removed = true;
                    notesNotificationIterator.remove();
                }//not coming soon*/

                if (!profile.containsAttribute(item.getAttributeId())) {
                    notesNotificationIterator.remove();
                    removed = true;
                    Log.e(ProfileDashboard.class.getName(), "Removed notification");
                }

                if (!removed) profile.getNotifications().add(item);
            }
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            if (defaultSharedPreferences.getBoolean("notifications_show_tips", true) && !profile.getNotifications().isEmpty()) {
                Log.i(ProfileDashboard.class.getName(), "Showing random tip");
                Notification n = profile.getNotifications().get(new Random().nextInt(profile.getNotifications().size()));
                if (n != null && n.getTip() != null && n.getTip().getMessage() != null)
                    Toast.makeText(this, n.getTip().getMessage(), Toast.LENGTH_SHORT).show();
            }
            getSupportActionBar().setTitle(profile.getName());
            addListeners();
        }
    }

    private void addListeners() {
        icon.setImageResource(MainActivity.getProfileImageResource(profile.getRelation()));

        //show notifications for this profile
        ProfileItemsAdapter notifiactionsAdapter = new ProfileItemsAdapter(this, profile, getAttributes(), profile.getNotifications());
        notifications.setAdapter(notifiactionsAdapter);

        //show message if no notifications exist
        if (profile.getNotifications().isEmpty()) {
            footer = (TextView) getLayoutInflater().inflate(R.layout.description_header_layout, null);
            footer.setText(R.string.no_pending_notifications);
            notifications.addFooterView(footer);
        } else {
            if (footer != null) notifications.removeFooterView(footer);
        }

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog dialog = new MaterialDialog.Builder(ProfileDashboard.this)
                        .title(R.string.details)
                        .customView(R.layout.detail_info_layout, true).build();
                TextView gender = (TextView) dialog.findViewById(R.id.gender);
                TextView relation = (TextView) dialog.findViewById(R.id.relation);
                TextView hairColor = (TextView) dialog.findViewById(R.id.hair_color);
                TextView eyeColor = (TextView) dialog.findViewById(R.id.eye_color);
                gender.setTextColor(ContextCompat.getColor(ProfileDashboard.this, profile.getGender().equals(getString(R.string.male)) ? R.color.RoyalBlue : R.color.Magenta));
                gender.setText(profile.getGender());
                relation.setText(profile.getRelation());
                hairColor.setText("Hair color: " + profile.getHairColor());
                eyeColor.setText("Eye color: " + profile.getEyeColor());
                dialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.edit) {
            Intent intent = new Intent(ProfileDashboard.this, AddProfileActivity.class);
            //put profile to be edited in intent
            intent.putExtra(AddProfileActivity.PROFILE, (Parcelable) profile);
            startActivity(intent);
        } else if (id == R.id.delete) {
            //already checked not to be notified
            if (Constants.config.getDeleteImmediatelly(ProfileDashboard.this)) {
                profile.delete();
                MainActivity.getInstance().removeProfile(profile);
                finish();
            } else {
                MaterialDialog dialog = new MaterialDialog.Builder(ProfileDashboard.this)
                        .title(getString(R.string.delete_profile).toUpperCase())
                                //.content(R.string.confirm_delete_profile)
                        .positiveText(android.R.string.yes)
                        .negativeText(android.R.string.cancel)
                        .cancelable(true)
                        .autoDismiss(true)
                        .customView(R.layout.do_not_ask_delete, true)
                        .iconRes(R.drawable.delete)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                profile.delete();
                                MainActivity.getInstance().removeProfile(profile);
                                finish();
                            }
                        })
                        .build();
                CheckBox doNotAskAgain = (CheckBox) dialog.findViewById(R.id.do_not_ask);
                doNotAskAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Constants.config.setDeleteImmediatelly(ProfileDashboard.this, isChecked);
                    }
                });
                dialog.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Push all ttributes in one list for showing
     *
     * @return List containing all attributes
     */
    private List<Attribute> getAttributes() {
        List<Attribute> attributes = new LinkedList<>();
        if (profile.getBirthday() != null) attributes.add(profile.getBirthday());
        if (profile.getWeddingDay() != null) attributes.add(profile.getWeddingDay());
        if (profile.getHeight() != null) attributes.add(profile.getHeight());
        if (profile.getWeight() != null) attributes.add(profile.getWeight());
        if (profile.getFavouriteFlowers() != null) attributes.add(profile.getFavouriteFlowers());
        if (profile.getShoeSize() != null) attributes.add(profile.getShoeSize());
        if (profile.getShirtSize() != null) attributes.add(profile.getShirtSize());
        if (profile.getFavouriteColor() != null) attributes.add(profile.getFavouriteColor());
        if (profile.getMenstrualCalendar() != null) attributes.add(profile.getMenstrualCalendar());
        if (profile.getTrousersSize() != null) attributes.add(profile.getTrousersSize());
        if (profile.gettShirtSize() != null) attributes.add(profile.gettShirtSize());
        if (profile.getFavouriteBook() != null) attributes.add(profile.getFavouriteBook());
        if (profile.getFavouriteDrink() != null) attributes.add(profile.getFavouriteDrink());
        if (profile.getFavouriteFood() != null) attributes.add(profile.getFavouriteFood());
        if (profile.getFavouriteMusic() != null) attributes.add(profile.getFavouriteMusic());
        return attributes;
    }

}
