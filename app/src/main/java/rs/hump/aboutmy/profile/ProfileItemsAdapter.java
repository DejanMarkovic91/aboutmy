package rs.hump.aboutmy.profile;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rs.hump.aboutmy.Constants;
import rs.hump.aboutmy.R;
import rs.hump.aboutmy.domain.Notification;
import rs.hump.aboutmy.domain.Profile;
import rs.hump.aboutmy.domain.ProfileItem;
import rs.hump.aboutmy.domain.attributes.Attribute;
import rs.hump.aboutmy.domain.attributes.MenstrualCalendar;
import rs.hump.aboutmy.domain.attributes.ShoeSize;

/**
 * Adapter for notifications for single profile
 *
 * @author Dejan Markovic
 */
public class ProfileItemsAdapter extends BaseAdapter {

    private Activity activity;
    private Profile profile;
    private List<ProfileItem> items;

    public ProfileItemsAdapter(Activity activity, Profile profile, List<Attribute> attributes, List<Notification> notifications) {
        this.activity = activity;
        this.profile = profile;
        this.items = new ArrayList<>();
        items.addAll(attributes);
        items.addAll(notifications);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View recycledView = convertView;
        if (getItemViewType(position) == 0) {
            if (recycledView == null) {
                recycledView = activity.getLayoutInflater().inflate(R.layout.notification_child_item, null);
                recycledView.setPadding(0, recycledView.getPaddingTop(), 0, recycledView.getPaddingBottom());
            }

            Notification item = (Notification) getItem(position);
            TextView msg = (TextView) recycledView;
            msg.setText(String.format("%s %s", Constants.dateFormat.format(new Date(item.getDueTime())), item.getMessage()));
        } else {
            Attribute item = (Attribute) getItem(position);

            if (recycledView == null) {
                recycledView = activity.getLayoutInflater().inflate(R.layout.simple_attribute, null);
                recycledView.setPadding(0, recycledView.getPaddingTop(), 0, recycledView.getPaddingBottom());
            }

            ImageView icon = (ImageView) recycledView.findViewById(R.id.icon);
            if (item instanceof MenstrualCalendar) {
                MenstrualCalendar mc = (MenstrualCalendar) item;
                icon.setImageResource(mc.getCurrentDayResource());
            }
            if (item instanceof ShoeSize && profile.getGender().equals(activity.getString(R.string.female))) {
                icon.setImageResource(R.drawable.ic_heel);
            } else {
                icon.setImageDrawable(item.getAttributeIconResource(activity));
            }
            //set attribute description
            TextView description = (TextView) recycledView.findViewById(R.id.description);
            description.setText(item.getTextualRepresentation());

        }
        return recycledView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position) instanceof Notification ? 0 : 1;
    }
}
