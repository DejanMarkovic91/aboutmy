package rs.hump.aboutmy.settings;

import android.app.backup.BackupAgentHelper;
import android.app.backup.FileBackupHelper;
import android.content.Context;

/**
 * Handles backuping and restoring of data on phone.
 *
 * @author Dejan Markovic
 */
public class BackupHelper extends BackupAgentHelper {

    private static final String DATABASE = "rs.hump.aboutmy.settings.database";
    private static final String DATABASE_JOURNAL = "rs.hump.aboutmy.settings.database_journal";
    private static final String NOTIFICATIONS = "rs.hump.aboutmy.settings.notifications";
    private static final String PENDING_NOTIFICATIONS = "rs.hump.aboutmy.settings.pending_notifications";
    private static final String ICU = "rs.hump.aboutmy.settings.ICU";

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseBackupHelper databaseBackupHelper = new DatabaseBackupHelper(this, "about.db");
        addHelper(DATABASE, databaseBackupHelper);
        DatabaseBackupHelper databaseJournalHelper = new DatabaseBackupHelper(this, "about.db-journal");
        addHelper(DATABASE_JOURNAL, databaseJournalHelper);
        FileBackupHelper fileBackupHelper = new FileBackupHelper(this, getFilesDir() + "/icu/icudt46l.bin");
        addHelper(ICU, fileBackupHelper);
        FileBackupHelper fileBackupHelper1 = new FileBackupHelper(this, getFilesDir() + "/notif.bin");
        addHelper(NOTIFICATIONS, fileBackupHelper1);
        FileBackupHelper fileBackupHelper2 = new FileBackupHelper(this, getFilesDir() + "/pending_notif.bin");
        addHelper(PENDING_NOTIFICATIONS, fileBackupHelper2);
    }

    /**
     * Database backup helper.
     */
    public class DatabaseBackupHelper extends FileBackupHelper {

        public DatabaseBackupHelper(Context ctx, String dbName) {
            super(ctx, ctx.getDatabasePath(dbName).getAbsolutePath());
        }
    }
}
