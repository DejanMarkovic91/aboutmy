package rs.hump.aboutmy.settings;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.Preference;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import java.util.Calendar;

/**
 * Budzevina za vremenski period.
 *
 * @author Dejan Markovic
 */
public class TimePreference extends Preference {

    private static int instanceCounter = 0;
    private int number = instanceCounter++;
    private java.text.DateFormat format;

    public TimePreference(Context context) {
        super(context);
        setCustomSummary();
    }

    public TimePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomSummary();
    }

    public TimePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomSummary();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TimePreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setCustomSummary();
    }

    @Override
    public void setSummary(CharSequence summary) {
        if (!summary.equals(""))//nabudz da se pojavi summary
            super.setSummary(summary);
    }

    public void setCustomSummary() {
        if(format==null){
            format = DateFormat.getTimeFormat(SettingsActivity.getInstance());
        }
        if (number % 2 == 1) {
            //9:00
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 9);
            cal.set(Calendar.MINUTE, 0);
            setSummary(format.format(cal.getTime()));
        } else {
            //21:00
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 21);
            cal.set(Calendar.MINUTE, 0);
            setSummary(format.format(cal.getTime()));
        }
    }

}
