package rs.hump.aboutmy.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Config {

    public boolean getDeleteImmediatelly(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("delete_immediately", false);
    }

    public void setDeleteImmediatelly(Context context, boolean flag) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        defaultSharedPreferences.edit().putBoolean("delete_immediately", flag);
    }

}